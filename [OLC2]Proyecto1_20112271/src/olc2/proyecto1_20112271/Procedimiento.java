package olc2.proyecto1_20112271;

import java.util.ArrayList;

public class Procedimiento {
    public String nombre = "";
    public String tipo = "";
    public String acceso = "";
    public ArrayList<Variable> globales = new ArrayList<Variable>();
    public ArrayList<Variable> parametros = new ArrayList<Variable>();
    public Object valor = null;
}
