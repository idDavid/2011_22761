package olc2.proyecto1_20112271;

import java.util.ArrayList;

public class Produccion{
    public int reduccion;/**/
    public String padre;/**/
    public ArrayList<Elemento> cuerpo;/**/
    public boolean completado;/* */
    public boolean reducido;

    public Produccion(int reduccion, String padre, boolean completado){
        this.reduccion = reduccion;
        this.padre = padre;
        this.cuerpo = new  ArrayList<Elemento>();
        this.completado = completado;
        this.reducido = false;
    }
}
