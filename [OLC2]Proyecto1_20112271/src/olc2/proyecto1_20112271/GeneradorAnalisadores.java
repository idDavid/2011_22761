package olc2.proyecto1_20112271;

import java.util.ArrayList;
import java.util.Objects;

public class GeneradorAnalisadores {

	public SLR metodo;
	public AnalizadorSintactico sintacticoUPG;
	public AnalizadorSintactico sintacticoXML;
	public ArrayList<String> erroresLexicos;       // Tipo, lexema, comentario, fila, columna
	public ArrayList<String> erroresSintacticos;   // Tipo, lexema, comentario, fila, columna
	public ArrayList<String> erroresSemanticos;    // Tipo, lexema, comentario, fila, columna
	public ArrayList<Clase> clases;
	
	public GeneradorAnalisadores(){
		this.metodo = new SLR();
		this.sintacticoUPG = new AnalizadorSintactico();
		this.sintacticoXML = new AnalizadorSintactico();
		this.erroresLexicos = new ArrayList<>();
		this.erroresSintacticos = new ArrayList<>();
		this.erroresSemanticos = new ArrayList<>();
		this.clases = new ArrayList<Clase>();
	}

	public void parcearUPG(String archivo){
		this.sintacticoUPG.AnalizarGramatica(archivo);
		if(this.sintacticoUPG.raiz != null){
			// this.sintacticoUPG.GraficarSintactico(this.sintacticoUPG.raiz);
            Variable variable = new Variable();
            variable = CONDICION(this.sintacticoUPG.raiz.hijos.get(0).hijos.get(2).hijos.get(1).hijos.get(0).hijos.get(0).hijos.get(2),variable);
            System.out.println(variable.valor);
            System.out.println(variable.tipo);
            System.out.println("********************");


			// obtenerInfoGramatica(this.sintacticoUPG.raiz,metodo,"");

			// System.out.print("\nTerminales: ");
			// for(int i=0; i<metodo.listaTerminales.size(); i++){
			//	 System.out.print("["+metodo.listaTerminales.get(i)+"]");
			// }

			// System.out.print("\nNoTerminales: ");
			// for(int i=0; i<metodo.listaNoTerminales.size(); i++){
			//	 System.out.print("["+metodo.listaNoTerminales.get(i)+"]");
			// }

			// System.out.print("\nPresedencia: ");
			// for(int i=0; i<metodo.listaPresedencia.size(); i++){
			//	 System.out.print("["+metodo.listaPresedencia.get(i)+"]");
			// }

			// System.out.print("\nAsociatividad: ");
			// for(int i=0; i<metodo.listaAsociatividad.size(); i++){
			//	 System.out.print("["+metodo.listaAsociatividad.get(i)+"]");
			// }

			// System.out.print("\nInicial: "+metodo.inicial);	 
			
			// System.out.print("\n\nGramatica: ");
			// for(int i=0; i<metodo.gramatica.size(); i++){
			//	 System.out.print("\n\t"+metodo.gramatica.get(i).padre +" -> ");
			//	 for(int j=0; j<metodo.gramatica.get(i).cuerpo.size(); j++){
			//		 System.out.print(metodo.gramatica.get(i).cuerpo.get(j).nombre+" ");
			//	 }
			// }

			// metodo.PrimerosSiguientes();
			// metodo.estados();

			// System.out.print("\n\nPrimeros: ");
			// for(int i=0; i<metodo.listaPrimeros.size(); i++){
			//	 String split[] = metodo.listaPrimeros.get(i).split(",");
			//	 System.out.print("\n\tP("+split[0]+") = {");
			//	 for(int j=1; j<split.length; j++){
			//		 System.out.print(split[j]+" ");
			//	 }
			//	 System.out.print("}");
			// }
			
			// System.out.print("\n\nSiguientes: ");
			// for(int i=0; i<metodo.listaSiguientes.size(); i++){
			//	 String split[] = metodo.listaSiguientes.get(i).split(",");
			//	 System.out.print("\n\tS("+split[0]+") = {");
			//	 for(int j=1; j<split.length; j++){
			//		 System.out.print(split[j]+" ");
			//	 }
			//	 System.out.print("}");
			// }

			// for(int n=0; n<metodo.listaEstados.size(); n++){
			//	 System.out.print("\n\n********************************************************************** "+metodo.listaEstados.get(n).completado);
			//	 System.out.print("\nEstado "+metodo.listaEstados.get(n).numero+": ");
			//	 for(int i=0; i<metodo.listaEstados.get(n).gotos.size(); i++){
			//		  System.out.print("("+metodo.listaEstados.get(n).gotos.get(i)+") ");
			//	 }
			//	 System.out.print("\n{");
			//	 for(int i=0; i<metodo.listaEstados.get(n).salidas.size(); i++){
			//		  System.out.print(metodo.listaEstados.get(n).salidas.get(i)+" ");
			//	 }
			//	 System.out.print("}\n");
			//	 for(int i=0; i<metodo.listaEstados.get(n).producciones.size(); i++){
			//		 System.out.print("\n\t"+metodo.listaEstados.get(n).producciones.get(i).reduccion+"   "+metodo.listaEstados.get(n).producciones.get(i).padre +" -> ");
			//		 for(int j=0; j<metodo.listaEstados.get(n).producciones.get(i).cuerpo.size(); j++){
			//			 Elemento elemento = metodo.listaEstados.get(n).producciones.get(i).cuerpo.get(j);
			//			 System.out.print(elemento.puntoIsq+elemento.nombre+elemento.puntoDer+" ");
			//		 }
			//	 }
			// }
			
			// metodo.generarTabla();
			// // metodo.verTablaTransiciones();
			
			// System.out.println("\n\n");
		}else{
			// Errores en UPG - importaciones
		}
	}
	
	public void parsearXML(String archivo){
		this.sintacticoXML.AnalizarGramatica(archivo);

		if(this.sintacticoXML.raiz != null){
		   // this.sintacticoXML.GraficarSintactico(this.sintacticoXML.raiz);

		   metodo = obtenerCadenaEntrada(this.sintacticoXML.raiz, metodo);

		   System.out.print("\n\nCadena de Entrada: ");
		   for(int i=0; i<metodo.cadenaEntrada.size(); i++){
			   System.out.print(metodo.cadenaEntrada.get(i).lexema+" ");
		   }

		   metodo.validarCadena();
		   // metodo.verCadenaPila();
		   metodo.GraficarArbol();
		}else{
			// Errores en xml
		}
	}
		
	public SLR obtenerInfoGramatica(Nodo padre, SLR slr, String productor){
		switch(padre.token){
			case "SX":
				slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
				break;
			case "GRAMAR":				  // GRAMAR			   ::=	 DECLARACIONES GRAMATICA SSS
				slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
				slr = obtenerInfoGramatica(padre.hijos.get(1),slr,productor);
				slr = obtenerInfoGramatica(padre.hijos.get(2),slr,productor);
				break;
			case "DECLARACIONES":
				switch(padre.hijos.size()){
					case 2:				 //  DECLARACIONES	   ::=	 DECLARACIONES DECLAR
						slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
						slr = obtenerInfoGramatica(padre.hijos.get(1),slr,productor);
						break;
					case 1:				 //  DECLARACIONES	   ::=	 DECLAR
						slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
						break;
				}
				break;
			case "DECLAR":
				switch(padre.hijos.get(0).token){
					case "DECTERMINAL":	 //  DECLAR			  ::=	 DECTERMINAL
						String split[] = padre.hijos.get(0).lexema.split(",");
						for(int i=0; i<split.length; i++){
							slr.listaTerminales.add(split[i]);
						}
						break;
					case "DECNOTERMINAL":   //  DECLAR			  ::=	 DECNOTERMINAL
						split = padre.hijos.get(0).lexema.split(",");
						for(int i=0; i<split.length; i++){
							slr.listaNoTerminales.add(split[i]);
						}
						break;
					case "PRECEDENCIA":	 //  DECLAR			  ::=	 PRECEDENCIA
						slr.listaPresedencia.add(padre.hijos.get(0).lexema);
						break;
					case "ASOCIATIVIDAD":	//  DECLAR			  ::=	 ASOCITIVIDAD
						split = padre.hijos.get(0).lexema.split(",");
						for(int i=1; i<split.length; i++){
							slr.listaAsociatividad.add(split[i]+"-"+split[0]);
						}
						break;
					case "INICIAL":		 //  DECLAR			  ::=	 INICIAL
						slr.inicial = padre.hijos.get(0).hijos.get(0).lexema;
						break;
				}
				break;
			case "GRAMATICA":
				switch(padre.hijos.size()){
					case 2:				 //  GRAMATICA		   ::=	 GRAMATICA DERIVACION
						slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
						slr = obtenerInfoGramatica(padre.hijos.get(1),slr,productor);
						break;
					case 1:				 //  GRAMATICA		   ::=	 DERIVACION
						slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
						break;
				}
				break;
			case "DERIVACION":			  //  DERIVACION		  ::=	 id PRODUCCION("PRODUCTOS","|")
				String derivador = padre.hijos.get(0).lexema;
				slr = obtenerInfoGramatica(padre.hijos.get(1),slr,derivador);
				break;
			case "PRODUCTOS":
				Produccion produccion = new Produccion(0,productor,false);
				for(int i=0; i<padre.hijos.size(); i++){
					Elemento elemento = new Elemento("","","","","","");
					
					if(i==0 && padre.hijos.get(0).token=="ACCION"){
						elemento.codIsq = padre.hijos.get(0);
						elemento.cadenaIsq = padre.hijos.get(0).cadenaIsq;
						i = 1;
					}

					elemento.nombre = padre.hijos.get(i).lexema;

					if((i+1) <= padre.hijos.size()-1){
						if(padre.hijos.get(i+1).token=="ACCION"){
							elemento.codDer = padre.hijos.get(i+1);
							elemento.cadenaDer = padre.hijos.get(i+1).cadenaIsq;
							i += 1;
						}
					}

					produccion.cuerpo.add(elemento);
				}
				slr.gramatica.add(produccion);

				break;
			case "|":
				slr = obtenerInfoGramatica(padre.hijos.get(0),slr,productor);
				slr = obtenerInfoGramatica(padre.hijos.get(1),slr,productor);
				break;




			case "SSS":

				break;
		}
		return slr;
	}

	public SLR obtenerCadenaEntrada(Nodo padre, SLR slr){
		switch(padre.token){
			case "SX":
				slr = obtenerCadenaEntrada(padre.hijos.get(0),slr);
				break;
			case "XML":
				slr = obtenerCadenaEntrada(padre.hijos.get(0),slr);
				break;
			case "SALIDAS":
				switch(padre.hijos.size()){
					case 2:
						slr = obtenerCadenaEntrada(padre.hijos.get(0),slr);
						slr = obtenerCadenaEntrada(padre.hijos.get(1),slr);
						break;
					case 1:
						slr = obtenerCadenaEntrada(padre.hijos.get(0),slr);
						break;
				}
				break;
			case "SALIDA":
				padre.token = padre.hijos.get(0).lexema;
				padre.hijos.clear();
				slr.cadenaEntrada.add(padre);
				break;
		}
		return slr;
	}

	public void obtenerClases(Nodo padre, boolean instancia){
		switch(padre.token){
			case "SX":
				padre.hijos.get(0);
				break;
			case "GRAMAR":  // GRAMAR			   ::=	 DECLARACIONES GRAMATICA SSS
				padre.hijos.get(2);
				break;
			case "SSS":
				switch(padre.hijos.size()){
					case 2: // SSS				  ::=	 SSS CODIGO
						obtenerClases(padre.hijos.get(0),instancia);
						obtenerClases(padre.hijos.get(1),instancia);
						break;
					case 1: // SSS				  ::=	 CODIGO
						obtenerClases(padre.hijos.get(0),instancia);
						break;	   
				}
				break;
			case "CODIGO":
				obtenerClases(padre.hijos.get(0),instancia);
				break;
			case "IMPORTACION":
				obtenerClases(padre.hijos.get(0),instancia);
				break;
			case "ARCHIVOS":
				switch(padre.hijos.size()){
					case 2: // ARCHIVOS		 ::=	 ARCHIVOS ARCHIVO
						obtenerClases(padre.hijos.get(0),instancia);
						obtenerClases(padre.hijos.get(1),instancia);
						break;
					case 1: // ARCHIVOS		 ::=	 ARCHIVO
						obtenerClases(padre.hijos.get(0),instancia);
						break;	   
				}
				break;
			case "ARCHIVO":
				obtenerClases(padre.hijos.get(0),instancia);
				break;
			case "GLOBAL":

				break;
			case "PROCEDIMIENTO":

				break;
			case "PRIVILEGIO":

				break;
			case "PARAMETROSDEC":

				break;
			case "CLASE":
				switch(padre.hijos.size()){
					case 3: // CLASE				::=	  id id INSTRUCCIONES
						if(instancia){
							boolean existe = false;
							for(int i=0; i<this.clases.size(); i++){
								if(this.clases.get(i).nombre.equals(padre.hijos.get(1).lexema)){
									existe = true;
									break;
								}
							}
							if(existe){
								existe = false;
								for(int i=0; i<this.clases.size(); i++){
									if(this.clases.get(i).nombre.equals(padre.hijos.get(0).lexema)){
										existe = true;
										break;
									}
								}
								if(!existe){
									Clase clase = new Clase();
									clase.nombre = padre.hijos.get(0).lexema;
									clase.cuerpo = padre.hijos.get(2);
								}else{
									agregarError("Semantico",padre.hijos.get(0).lexema,"La clase "+padre.hijos.get(0).lexema+" existe",padre.hijos.get(0).fila,padre.hijos.get(0).columna);
								}
							}else{
								agregarError("Semantico",padre.hijos.get(1).lexema,"La clase "+padre.hijos.get(1).lexema+" no existe",padre.hijos.get(1).fila,padre.hijos.get(1).columna);
							}
						}
						break;
					case 1: // CLASE				::=	  id INSTRUCCIONES
						if(!instancia){
							boolean existe = false;
							for(int i=0; i<this.clases.size(); i++){
								if(this.clases.get(i).nombre.equals(padre.hijos.get(0).lexema)){
									existe = true;
									break;
								}
							}
							if(!existe){
								Clase clase = new Clase();
								clase.nombre = padre.hijos.get(0).lexema;
								clase.cuerpo = padre.hijos.get(1);
							}else{
								agregarError("Semantico",padre.hijos.get(0).lexema,"La clase "+padre.hijos.get(0).lexema+" existe",padre.hijos.get(0).fila,padre.hijos.get(0).columna);
							}
						}
						break;	   
				}
				break;
			case "INSTRUCCIONES":

				break;
			case "INSTRUCCION":

				break;
			case "CONSTRUCTOR":

				break;
			
		}
	}

	public void infoClases(Nodo padre, Clase clase, boolean instancia){
		switch(padre.token){
			case "INSTRUCCIONES":
				switch(padre.hijos.size()){
					case 2: // INSTRUCCIONES		::=	 INSTRUCCIONES INSTRUCCION
						obtenerClases(padre.hijos.get(0),instancia);
						obtenerClases(padre.hijos.get(1),instancia);
						break;
					case 1: // INSTRUCCIONES		::=	 INSTRUCCION
						obtenerClases(padre.hijos.get(0),instancia);
						break;	   
				}
				break;
			case "INSTRUCCION":
				switch(padre.hijos.size()){
					case 2: // INSTRUCCION		  ::=	 tOverride PROCEDIMIENTO 
						if(instancia){
							// Si se realisa el override
						}else{
							agregarError("Semantico",clase.nombre,"La clase "+clase.nombre+" no puede hacer Override si no extiende de otra",padre.hijos.get(0).fila,padre.hijos.get(0).fila);
						}
						break;
					case 1: // INSTRUCCION		  ::=	 GLOBAL | PROCEDIMIENTO  | CONSTRUCTOR | CLASE
						obtenerClases(padre.hijos.get(0),instancia);
						break;	   
				}
				break;
			case "CONSTRUCTOR":
				switch(padre.hijos.size()){
					case 3: // CONSTRUCTOR		  ::=	 id PARAMETROSDEC SENTENCIAS
						if(instancia){
							// Si se realisa el override
						}
						break;
					case 2: // CONSTRUCTOR		  ::=	 id SENTENCIAS
						obtenerClases(padre.hijos.get(0),instancia);
						break;	   
				}
				break;
			case "GLOBAL":
				switch(padre.hijos.size()){
					case 2: // GLOBAL			   ::=	 PRIVILEGIO DECLARACION

						break;
					case 1: // GLOBAL			   ::=	 DECLARACION

						break;	   
				}
				break;
			case "DECLARACION":
				
				break;
			case "PROCEDIMIENTO":

				break;
			case "PRIVILEGIO":

				break;
			case "PARAMETROSDEC":

				break;
			
		}
	}

    public Variable E(Nodo padre, Variable variable){
        switch(padre.token){
            case "E":
                switch(padre.hijos.size()){
                    case 3:
                        Variable valor1 = new Variable();
                        Variable valor2 = new Variable();
                        valor1 = E(padre.hijos.get(0), valor1);
                        valor2 = E(padre.hijos.get(2), valor2);

                        switch (padre.hijos.get(1).token){
                            case "tMas":  /** E + ToTerm("+") + E **/
                                if (valor1.valor != null && valor2.valor != null ){
                                    if(valor1.tipo.equals(valor2.tipo)){
                                        if(valor1.tipo.equals("String")){
                                            String ope1 = (String)valor1.valor;
                                            String ope2 = (String)valor2.valor;
                                            variable.valor = ope1 + ope2;
                                            variable.tipo = "String";
                                        }else if(valor1.tipo.equals("Integer")){
                                            Integer ope1 = (Integer)valor1.valor;
                                            Integer ope2 = (Integer)valor2.valor;
                                            variable.valor = ope1 + ope2;
                                            variable.tipo = "Integer";
                                        }else if(valor1.tipo.equals("Double")){
                                            Double ope1 = Double.valueOf(valor1.valor.toString());
                                            Double ope2 = Double.valueOf(valor2.valor.toString());
                                            variable.valor = ope1 + ope2;
                                            variable.tipo = "Double";
                                        }else if(valor1.tipo.equals("Boolean")){
                                            Boolean ope1 = Boolean.valueOf(valor2.valor.toString());
                                            Boolean ope2 = (Boolean)valor2.valor;
                                            variable.valor = ope1 || ope2;
                                            variable.tipo = "Boolean";
                                        }else{
                                            // Error char char
                                            System.out.println("Error en tipo de dato sumar char char");
                                        }
                                    }else{
                                        if(valor1.tipo.equals("String") || valor2.tipo.equals("String")){
                                            if(!( (valor1.tipo.equals("Boolean")) || (valor2.tipo.equals("Boolean")) ) ){
                                                
                                                
                                                String ope1 = valor1.valor.toString();
                                                String ope2 = valor2.valor.toString();
                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "String";
                                            }else{
                                                // Error Boolean
                                                System.out.println("Error en tipo de dato concatenar Boolean");
                                            }
                                        }else if(valor1.tipo.equals("Double") || valor2.tipo.equals("Double")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1.0;
                                                    }else{
                                                        ope1 = 0.0;
                                                    }
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1.0;
                                                    }else{
                                                        ope2 = 0.0;
                                                    }
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "Double";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Double.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Double)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Double.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Double)valor2.valor;
                                                }

                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "Double";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "Double";
                                            }
                                        }else if(valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                int ope1;
                                                int ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1;
                                                    }else{
                                                        ope1 = 0;
                                                    }
                                                }else{
                                                    ope1 = Integer.parseInt(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1;
                                                    }else{
                                                        ope2 = 0;
                                                    }
                                                }else{
                                                    ope2 = Integer.parseInt(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "Integer";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Integer ope1;
                                                Integer ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Integer.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Integer)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Integer.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Integer)valor2.valor;
                                                }

                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "Integer";
                                            }else{
                                                Integer ope1 = (Integer)valor1.valor;
                                                Integer ope2 = (Integer)valor2.valor;
                                                variable.valor = ope1 + ope2;
                                                variable.tipo = "Integer";
                                            }
                                        }
                                    }
                                }
                                break;  
                            case "tMenos":  /** E + ToTerm("-") + E **/
                                if (valor1.valor != null && valor2.valor != null ){
                                    if(valor1.tipo.equals(valor2.tipo)){
                                        if(valor1.tipo.equals("String")){
                                            // Errror
                                            System.out.println("No se puede tMen un String"); 
                                        }else if(valor1.tipo.equals("Integer")){
                                            Integer ope1 = (Integer)valor1.valor;
                                            Integer ope2 = (Integer)valor2.valor;
                                            variable.valor = ope1 - ope2;
                                            variable.tipo = "Integer";
                                        }else if(valor1.tipo.equals("Double")){
                                            Double ope1 = Double.valueOf(valor1.valor.toString());
                                            Double ope2 = Double.valueOf(valor2.valor.toString());
                                            variable.valor = ope1 - ope2;
                                            variable.tipo = "Double";
                                        }else if(valor1.tipo.equals("Boolean")){
                                            // Error
                                            System.out.println("Error en tipo de dato");
                                        }else{
                                            // Error char char
                                            System.out.println("Error en tipo de dato");
                                        }
                                    }else{
                                        if(valor1.tipo.equals("String") || valor2.tipo.equals("String")){
                                            // Error Boolean
                                            System.out.println("Error en tipo de dato");
                                        }else if(valor1.tipo.equals("Double") || valor2.tipo.equals("Double")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1.0;
                                                    }else{
                                                        ope1 = 0.0;
                                                    }
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1.0;
                                                    }else{
                                                        ope2 = 0.0;
                                                    }
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 - ope2;
                                                variable.tipo = "Double";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Double.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Double)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Double.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Double)valor2.valor;
                                                }

                                                variable.valor = ope1 - ope2;
                                                variable.tipo = "Double";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = ope1 - ope2;
                                                variable.tipo = "Double";
                                            }
                                        }else if(valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                int ope1;
                                                int ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1;
                                                    }else{
                                                        ope1 = 0;
                                                    }
                                                }else{
                                                    ope1 = Integer.parseInt(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1;
                                                    }else{
                                                        ope2 = 0;
                                                    }
                                                }else{
                                                    ope2 = Integer.parseInt(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 - ope2;
                                                variable.tipo = "Integer";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Integer ope1;
                                                Integer ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Integer.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Integer)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Integer.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Integer)valor2.valor;
                                                }

                                                variable.valor = ope1 - ope2;
                                                variable.tipo = "Integer";
                                            }else{
                                                Integer ope1 = (Integer)valor1.valor;
                                                Integer ope2 = (Integer)valor2.valor;
                                                variable.valor = ope1 - ope2;
                                                variable.tipo = "Integer";
                                            }
                                        }
                                    }
                                }
                                break;  
                            case "tPor":  /** E + ToTerm("*") + E **/
                                if (valor1.valor != null && valor2.valor != null ){
                                    if(valor1.tipo.equals(valor2.tipo)){
                                        if(valor1.tipo.equals("String")){
                                            // Errror
                                            System.out.println("No se puede tPor un String"); 
                                        }else if(valor1.tipo.equals("Integer")){
                                            Integer ope1 = (Integer)valor1.valor;
                                            Integer ope2 = (Integer)valor2.valor;
                                            variable.valor = ope1 * ope2;
                                            variable.tipo = "Integer";
                                        }else if(valor1.tipo.equals("Double")){
                                            Double ope1 = Double.valueOf(valor1.valor.toString());
                                            Double ope2 = Double.valueOf(valor2.valor.toString());
                                            variable.valor = ope1 * ope2;
                                            variable.tipo = "Double";
                                        }else if(valor1.tipo.equals("Boolean")){
                                            Boolean ope1 = Boolean.valueOf(valor2.valor.toString());
                                            Boolean ope2 = (Boolean)valor2.valor;
                                            variable.valor = ope1 && ope2;
                                            variable.tipo = "Boolean";
                                        }else{
                                            // Error char char
                                            System.out.println("Error en tipo de dato");
                                        }
                                    }else{
                                        if(valor1.tipo.equals("String") || valor2.tipo.equals("String")){
                                            // Error Boolean
                                            System.out.println("Error en tipo de dato");
                                        }else if(valor1.tipo.equals("Double") || valor2.tipo.equals("Double")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1.0;
                                                    }else{
                                                        ope1 = 0.0;
                                                    }
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1.0;
                                                    }else{
                                                        ope2 = 0.0;
                                                    }
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 * ope2;
                                                variable.tipo = "Double";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Double.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Double)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Double.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Double)valor2.valor;
                                                }

                                                variable.valor = ope1 * ope2;
                                                variable.tipo = "Double";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = ope1 * ope2;
                                                variable.tipo = "Double";
                                            }
                                        }else if(valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                int ope1;
                                                int ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1;
                                                    }else{
                                                        ope1 = 0;
                                                    }
                                                }else{
                                                    ope1 = Integer.parseInt(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1;
                                                    }else{
                                                        ope2 = 0;
                                                    }
                                                }else{
                                                    ope2 = Integer.parseInt(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 * ope2;
                                                variable.tipo = "Integer";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Integer ope1;
                                                Integer ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Integer.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Integer)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Integer.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Integer)valor2.valor;
                                                }

                                                variable.valor = ope1 * ope2;
                                                variable.tipo = "Integer";
                                            }else{
                                                Integer ope1 = (Integer)valor1.valor;
                                                Integer ope2 = (Integer)valor2.valor;
                                                variable.valor = ope1 * ope2;
                                                variable.tipo = "Integer";
                                            }
                                        }
                                    }
                                }
                                break;  
                            case "tDiv":  /** E + ToTerm("/") + E **/
                                if (valor1.valor != null && valor2.valor != null ){
                                    if(valor1.tipo.equals(valor2.tipo)){
                                        if(valor1.tipo.equals("String")){
                                            // Errror
                                            System.out.println("No se puede tDiv un String"); 
                                        }else if(valor1.tipo.equals("Integer")){
                                            Double ope1 = Double.valueOf(valor1.valor.toString());
                                            Double ope2 = Double.valueOf(valor2.valor.toString());
                                            variable.valor = ope1 / ope2;
                                            variable.tipo = "Double";
                                        }else if(valor1.tipo.equals("Double")){
                                            Double ope1 = Double.valueOf(valor1.valor.toString());
                                            Double ope2 = Double.valueOf(valor2.valor.toString());
                                            variable.valor = ope1 / ope2;
                                            variable.tipo = "Double";
                                        }else if(valor1.tipo.equals("Boolean")){
                                            // Error
                                            System.out.println("Error en tipo de dato");
                                        }else{
                                            // Error char char
                                            System.out.println("Error en tipo de dato");
                                        }
                                    }else{
                                        if(valor1.tipo.equals("String") || valor2.tipo.equals("String")){
                                            // Error Boolean
                                            System.out.println("Error en tipo de dato");
                                        }else if(valor1.tipo.equals("Double") || valor2.tipo.equals("Double")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1.0;
                                                    }else{
                                                        ope1 = 0.0;
                                                    }
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1.0;
                                                    }else{
                                                        ope2 = 0.0;
                                                    }
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 / ope2;
                                                variable.tipo = "Double";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Double.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Double)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Double.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Double)valor2.valor;
                                                }

                                                variable.valor = ope1 / ope2;
                                                variable.tipo = "Double";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = ope1 / ope2;
                                                variable.tipo = "Double";
                                            }
                                        }else if(valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1.0;
                                                    }else{
                                                        ope1 = 0.0;
                                                    }
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1.0;
                                                    }else{
                                                        ope2 = 0.0;
                                                    }
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = ope1 / ope2;
                                                variable.tipo = "Double";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Double.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Double.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }

                                                variable.valor = ope1 / ope2;
                                                variable.tipo = "Double";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = ope1 / ope2;
                                                variable.tipo = "Double";
                                            }
                                        }
                                    }
                                }
                                break;  
                            case "tPot":  /** E + ToTerm("^") + E **/
                                if (valor1.valor != null && valor2.valor != null ){
                                    if(valor1.tipo.equals(valor2.tipo)){
                                        if(valor1.tipo.equals("String")){
                                            // Errror
                                            System.out.println("No se puede tPot un String"); 
                                        }else if(valor1.tipo.equals("Integer")){
                                            Integer ope1 = (Integer)valor1.valor;
                                            Integer ope2 = (Integer)valor2.valor;
                                            variable.valor = (int)Math.pow(ope1,ope2);
                                            variable.tipo = "Integer";
                                        }else if(valor1.tipo.equals("Double")){
                                            Double ope1 = Double.valueOf(valor1.valor.toString());
                                            Double ope2 = Double.valueOf(valor2.valor.toString());
                                            variable.valor = Math.pow(ope1,ope2);
                                            variable.tipo = "Double";
                                        }else if(valor1.tipo.equals("Boolean")){
                                            // Error
                                            System.out.println("Error en tipo de dato");
                                        }else{
                                            // Error char char
                                            System.out.println("Error en tipo de dato");
                                        }
                                    }else{
                                        if(valor1.tipo.equals("String") || valor2.tipo.equals("String")){
                                            // Error Boolean
                                            System.out.println("Error en tipo de dato");
                                        }else if(valor1.tipo.equals("Double") || valor2.tipo.equals("Double")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1.0;
                                                    }else{
                                                        ope1 = 0.0;
                                                    }
                                                }else{
                                                    ope1 = Double.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1.0;
                                                    }else{
                                                        ope2 = 0.0;
                                                    }
                                                }else{
                                                    ope2 = Double.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = Math.pow(ope1,ope2);
                                                variable.tipo = "Double";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Double ope1;
                                                Double ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Double.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Double)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Double.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Double)valor2.valor;
                                                }

                                                variable.valor = Math.pow(ope1,ope2);
                                                variable.tipo = "Double";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = Math.pow(ope1,ope2);
                                                variable.tipo = "Double";
                                            }
                                        }else if(valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer")){
                                            if(valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean")){
                                                Integer ope1;
                                                Integer ope2;
                                                if(valor1.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor1.valor.toString())){
                                                        ope1 = 1;
                                                    }else{
                                                        ope1 = 0;
                                                    }
                                                }else{
                                                    ope1 = Integer.valueOf(valor1.valor.toString());
                                                }
                                                if(valor2.tipo.equals("Boolean")){
                                                    if(Boolean.valueOf(valor2.valor.toString())){
                                                        ope2 = 1;
                                                    }else{
                                                        ope2 = 0;
                                                    }
                                                }else{
                                                    ope2 = Integer.valueOf(valor2.valor.toString());
                                                }
                                                
                                                variable.valor = (int)Math.pow(ope1,ope2);
                                                variable.tipo = "Integer";
                                            }else if(valor1.tipo.equals("Character") || valor2.tipo.equals("Character") ){
                                                Integer ope1;
                                                Integer ope2;
                                                if(valor1.tipo.equals("Character")){
                                                    ope1 = Integer.valueOf((Character)valor1.valor);
                                                }else{
                                                    ope1 = (Integer)valor1.valor;
                                                }
                                                if(valor2.tipo.equals("Character")){
                                                    ope2 = Integer.valueOf((Character)valor2.valor);
                                                }else{
                                                    ope2 = (Integer)valor2.valor;
                                                }

                                                variable.valor = (int)Math.pow(ope1,ope2);
                                                variable.tipo = "Integer";
                                            }else{
                                                Double ope1 = Double.valueOf(valor1.valor.toString());
                                                Double ope2 = Double.valueOf(valor2.valor.toString());
                                                variable.valor = Math.pow(ope1,ope2);
                                                variable.tipo = "Double";
                                            }
                                        }
                                    }
                                }
                                break;  
                        }
                        break;
                    case 2: 
                        switch(padre.hijos.get(1).token){
                            case "tIncremento": // E::= E tIncremento
                                valor1 = new Variable();
                                valor1 = E(padre.hijos.get(0), valor1);
                                if(valor1.tipo.equals("Character")){
                                    variable.valor = Integer.valueOf((Character)valor1.valor)+1;
                                    variable.tipo = "Integer";
                                }else if(valor1.tipo.equals("Double")){
                                    variable.valor = Double.valueOf(valor1.valor.toString())+1;
                                    variable.tipo = "Double";
                                }else if(valor1.tipo.equals("Integer")){
                                    variable.valor = Integer.valueOf(valor1.valor.toString())+1;
                                    variable.tipo = "Integer";
                                }else{
                                    // Error aumento
                                    System.out.println("Error en tipo de dato no se puede aumentar");
                                }
                                break;
                            case "tDecremento": // E::= E tDecremento
                                valor1 = new Variable();
                                valor1 = E(padre.hijos.get(0), valor1);
                                if(valor1.tipo.equals("Character")){
                                    variable.valor = Integer.valueOf((Character)valor1.valor)-1;
                                    variable.tipo = "Integer";
                                }else if(valor1.tipo.equals("Double")){
                                    variable.valor = Double.valueOf(valor1.valor.toString())-1;
                                    variable.tipo = "Double";
                                }else if(valor1.tipo.equals("Integer")){
                                    variable.valor = Integer.valueOf(valor1.valor.toString())-1;
                                    variable.tipo = "Integer";
                                }else{
                                    // Error aumento
                                    System.out.println("Error en tipo de dato no se puede decrementar");
                                }
                                break;
                            case "E":   // E::= tMenos E
                                valor1 = new Variable();
                                valor1 = E(padre.hijos.get(1), valor1);
                                if(valor1.tipo.equals("Integer")){
                                    variable.valor = -1*Integer.parseInt(valor1.valor.toString());
                                    variable.tipo = "Integer";
                                }else if(valor1.tipo.equals("Double")){
                                    variable.valor = -1*Double.valueOf(valor1.valor.toString());
                                    variable.tipo = "Double";
                                }else{
                                    // Error aumento
                                    System.out.println("Error en tipo de dato");
                                }
                                break;
                        }
                        break;
                    case 1: // E::= entero | doble | cadena | charr | tTrue | tFalse | CONDICION  | VARIABLE | LLAMADA | PROPIAS
                        variable = E(padre.hijos.get(0), variable);
                        break;
                }
                break;
            case "entero":
                variable.valor = Integer.parseInt(padre.lexema);
                variable.tipo = "Integer";
                break;
            case "doble":
                variable.valor = Double.valueOf(padre.lexema);
                variable.tipo = "Double";
                break;
            case "cadena":
                variable.valor = padre.lexema.replace("\"","");
                variable.tipo = "String";
                break;
            case "charr":
                variable.valor = Character.valueOf(padre.lexema.charAt(1));
                variable.tipo = "Character";
                break;
            case "tTrue":
                variable.valor = true;
                variable.tipo = "Boolean";
                break;
            case "tFalse":
                variable.valor = false;
                variable.tipo = "Boolean";
                break;
            case "CONDICION":
                Variable valor1 = new Variable();
                valor1 = CONDICION(padre.hijos.get(1), valor1);
                variable.valor = valor1.valor;
                variable.tipo = valor1.tipo;
                break; 
            case "VARIABLE":

                break;
            case "LLAMADA":

                break;
            case "PROPIAS":

                break;
        }
        return variable;
    }

	public Variable CONDICION(Nodo padre, Variable variable){
		switch(padre.token){
			case "CONDICION":
				switch(padre.hijos.size()){
					case 3:
						if(padre.hijos.get(1).token.equals("RELACIONAL")){  // CONDICION ::= E RELACIONAL E
							Variable valor1 = new Variable();
							Variable valor2 = new Variable();
							valor1 = E(padre.hijos.get(0), valor1);
							valor2 = E(padre.hijos.get(2), valor2);

							if (valor1.valor != null && valor2.valor != null ){
								if(!( (valor1.tipo.equals("Boolean") || valor2.tipo.equals("Boolean") ))) {
									switch(padre.hijos.get(1).hijos.get(0).token){
										case "tMayor":
											if(valor1.tipo.equals(valor2.tipo)){
												if(valor1.tipo.equals("String")){
													String ope1 = (String)valor1.valor;
													String ope2 = (String)valor2.valor;
													int n1 = 0;
													int n2 = 0;
													for (int i = 0; i < ope1.length(); i++) {
														n1 += ope1.charAt(i);
													}
													for (int i = 0; i < ope2.length(); i++) {
														n2 += ope2.charAt(i);
													}
													variable.valor = n1 > n2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Integer")){
													Integer ope1 = (Integer)valor1.valor;
													Integer ope2 = (Integer)valor2.valor;
													variable.valor = ope1 > ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Double")){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 > ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Character")){
													Character ope1 = (Character)valor1.valor;
													Character ope2 = (Character)valor2.valor;
													variable.valor = ope1 > ope2;
													variable.tipo = "Boolean";
												}
											}else{
												if( ((valor1.tipo.equals("String") || valor2.tipo.equals("String")) && (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) ) ) {
													if(valor1.tipo.equals("String")){
														String ope1 = (String)valor1.valor;
														Character ope2 = (Character)valor2.valor;
														int n1 = 0;
														for (int i = 0; i < ope1.length(); i++) {
															n1 += ope1.charAt(i);
														}
														variable.valor = n1 > ope2;
														variable.tipo = "Boolean";
													}else{
														Character ope1 = (Character)valor1.valor;
														String ope2 = (String)valor2.valor;
														int n2 = 0;
														for (int i = 0; i < ope2.length(); i++) {
															n2 += ope2.charAt(i);
														}
														variable.valor = ope1 > n2;
														variable.tipo = "Boolean";
													}
												}else if(( (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) && ( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ) ) ){
													 if(valor1.tipo.equals("Character")){
														Character ope1 = (Character)valor1.valor;
														Double ope2 = Double.valueOf(valor2.valor.toString());
														variable.valor = ope1 > ope2;
														variable.tipo = "Boolean";
													}else{
														Double ope1 = Double.valueOf(valor1.valor.toString());
														Character ope2 = (Character)valor2.valor;
														variable.valor = ope1 > ope2;
														variable.tipo = "Boolean";
													}
												}else if( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 > ope2;
													variable.tipo = "Boolean";
												}else{
													// Error String Numerico
												}
											}
											break;
										case "tMenor":
											if(valor1.tipo.equals(valor2.tipo)){
												if(valor1.tipo.equals("String")){
													String ope1 = (String)valor1.valor;
													String ope2 = (String)valor2.valor;
													int n1 = 0;
													int n2 = 0;
													for (int i = 0; i < ope1.length(); i++) {
														n1 += ope1.charAt(i);
													}
													for (int i = 0; i < ope2.length(); i++) {
														n2 += ope2.charAt(i);
													}
													variable.valor = n1 < n2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Integer")){
													Integer ope1 = (Integer)valor1.valor;
													Integer ope2 = (Integer)valor2.valor;
													variable.valor = ope1 < ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Double")){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 < ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Character")){
													Character ope1 = (Character)valor1.valor;
													Character ope2 = (Character)valor2.valor;
													variable.valor = ope1 < ope2;
													variable.tipo = "Boolean";
												}
											}else{
												if( ((valor1.tipo.equals("String") || valor2.tipo.equals("String")) && (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) ) ) {
													if(valor1.tipo.equals("String")){
														String ope1 = (String)valor1.valor;
														Character ope2 = (Character)valor2.valor;
														int n1 = 0;
														for (int i = 0; i < ope1.length(); i++) {
															n1 += ope1.charAt(i);
														}
														variable.valor = n1 < ope2;
														variable.tipo = "Boolean";
													}else{
														Character ope1 = (Character)valor1.valor;
														String ope2 = (String)valor2.valor;
														int n2 = 0;
														for (int i = 0; i < ope2.length(); i++) {
															n2 += ope2.charAt(i);
														}
														variable.valor = ope1 < n2;
														variable.tipo = "Boolean";
													}
												}else if(( (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) && ( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ) ) ){
													 if(valor1.tipo.equals("Character")){
														Character ope1 = (Character)valor1.valor;
														Double ope2 = Double.valueOf(valor2.valor.toString());
														variable.valor = ope1 < ope2;
														variable.tipo = "Boolean";
													}else{
														Double ope1 = Double.valueOf(valor1.valor.toString());
														Character ope2 = (Character)valor2.valor;
														variable.valor = ope1 < ope2;
														variable.tipo = "Boolean";
													}
												}else if( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 < ope2;
													variable.tipo = "Boolean";
												}else{
													// Error String Numerico
												}
											}
											break;
										case "tMayorIgual":
											if(valor1.tipo.equals(valor2.tipo)){
												if(valor1.tipo.equals("String")){
													String ope1 = (String)valor1.valor;
													String ope2 = (String)valor2.valor;
													int n1 = 0;
													int n2 = 0;
													for (int i = 0; i < ope1.length(); i++) {
														n1 += ope1.charAt(i);
													}
													for (int i = 0; i < ope2.length(); i++) {
														n2 += ope2.charAt(i);
													}
													variable.valor = n1 >= n2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Integer")){
													Integer ope1 = (Integer)valor1.valor;
													Integer ope2 = (Integer)valor2.valor;
													variable.valor = ope1 >= ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Double")){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 >= ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Character")){
													Character ope1 = (Character)valor1.valor;
													Character ope2 = (Character)valor2.valor;
													variable.valor = ope1 >= ope2;
													variable.tipo = "Boolean";
												}
											}else{
												if( ((valor1.tipo.equals("String") || valor2.tipo.equals("String")) && (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) ) ) {
													if(valor1.tipo.equals("String")){
														String ope1 = (String)valor1.valor;
														Character ope2 = (Character)valor2.valor;
														int n1 = 0;
														for (int i = 0; i < ope1.length(); i++) {
															n1 += ope1.charAt(i);
														}
														variable.valor = n1 >= ope2;
														variable.tipo = "Boolean";
													}else{
														Character ope1 = (Character)valor1.valor;
														String ope2 = (String)valor2.valor;
														int n2 = 0;
														for (int i = 0; i < ope2.length(); i++) {
															n2 += ope2.charAt(i);
														}
														variable.valor = ope1 >= n2;
														variable.tipo = "Boolean";
													}
												}else if(( (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) && ( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ) ) ){
													 if(valor1.tipo.equals("Character")){
														Character ope1 = (Character)valor1.valor;
														Double ope2 = Double.valueOf(valor2.valor.toString());
														variable.valor = ope1 >= ope2;
														variable.tipo = "Boolean";
													}else{
														Double ope1 = Double.valueOf(valor1.valor.toString());
														Character ope2 = (Character)valor2.valor;
														variable.valor = ope1 >= ope2;
														variable.tipo = "Boolean";
													}
												}else if( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 >= ope2;
													variable.tipo = "Boolean";
												}else{
													// Error String Numerico
												}
											}
											break;
										case "tMenorIgual":
											if(valor1.tipo.equals(valor2.tipo)){
												if(valor1.tipo.equals("String")){
													String ope1 = (String)valor1.valor;
													String ope2 = (String)valor2.valor;
													int n1 = 0;
													int n2 = 0;
													for (int i = 0; i < ope1.length(); i++) {
														n1 += ope1.charAt(i);
													}
													for (int i = 0; i < ope2.length(); i++) {
														n2 += ope2.charAt(i);
													}
													variable.valor = n1 <= n2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Integer")){
													Integer ope1 = (Integer)valor1.valor;
													Integer ope2 = (Integer)valor2.valor;
													variable.valor = ope1 <= ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Double")){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 <= ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Character")){
													Character ope1 = (Character)valor1.valor;
													Character ope2 = (Character)valor2.valor;
													variable.valor = ope1 <= ope2;
													variable.tipo = "Boolean";
												}
											}else{
												if( ((valor1.tipo.equals("String") || valor2.tipo.equals("String")) && (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) ) ) {
													if(valor1.tipo.equals("String")){
														String ope1 = (String)valor1.valor;
														Character ope2 = (Character)valor2.valor;
														int n1 = 0;
														for (int i = 0; i < ope1.length(); i++) {
															n1 += ope1.charAt(i);
														}
														variable.valor = n1 <= ope2;
														variable.tipo = "Boolean";
													}else{
														Character ope1 = (Character)valor1.valor;
														String ope2 = (String)valor2.valor;
														int n2 = 0;
														for (int i = 0; i < ope2.length(); i++) {
															n2 += ope2.charAt(i);
														}
														variable.valor = ope1 <= n2;
														variable.tipo = "Boolean";
													}
												}else if(( (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) && ( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ) ) ){
													 if(valor1.tipo.equals("Character")){
														Character ope1 = (Character)valor1.valor;
														Double ope2 = Double.valueOf(valor2.valor.toString());
														variable.valor = ope1 <= ope2;
														variable.tipo = "Boolean";
													}else{
														Double ope1 = Double.valueOf(valor1.valor.toString());
														Character ope2 = (Character)valor2.valor;
														variable.valor = ope1 <= ope2;
														variable.tipo = "Boolean";
													}
												}else if( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 <= ope2;
													variable.tipo = "Boolean";
												}else{
													// Error String Numerico
												}
											}
											break;
										case "tIgual":
											if(valor1.tipo.equals(valor2.tipo)){
												if(valor1.tipo.equals("String")){
													String ope1 = (String)valor1.valor;
													String ope2 = (String)valor2.valor;
													int n1 = 0;
													int n2 = 0;
													for (int i = 0; i < ope1.length(); i++) {
														n1 += ope1.charAt(i);
													}
													for (int i = 0; i < ope2.length(); i++) {
														n2 += ope2.charAt(i);
													}
													variable.valor = n1 == n2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Integer")){
													Integer ope1 = (Integer)valor1.valor;
													Integer ope2 = (Integer)valor2.valor;
													variable.valor = ope1 == ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Double")){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 == ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Character")){
													Character ope1 = (Character)valor1.valor;
													Character ope2 = (Character)valor2.valor;
													variable.valor = ope1 == ope2;
													variable.tipo = "Boolean";
												}
											}else{
												if( ((valor1.tipo.equals("String") || valor2.tipo.equals("String")) && (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) ) ) {
													if(valor1.tipo.equals("String")){
														String ope1 = (String)valor1.valor;
														Character ope2 = (Character)valor2.valor;
														int n1 = 0;
														for (int i = 0; i < ope1.length(); i++) {
															n1 += ope1.charAt(i);
														}
														variable.valor = n1 == ope2.hashCode();
														variable.tipo = "Boolean";
													}else{
														Character ope1 = (Character)valor1.valor;
														String ope2 = (String)valor2.valor;
														int n2 = 0;
														for (int i = 0; i < ope2.length(); i++) {
															n2 += ope2.charAt(i);
														}
														variable.valor = ope1.hashCode() == n2;
														variable.tipo = "Boolean";
													}
												}else if(( (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) && ( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ) ) ){
													 if(valor1.tipo.equals("Character")){
														Character ope1 = (Character)valor1.valor;
														Double ope2 = Double.valueOf(valor2.valor.toString());
														variable.valor = ope1.hashCode() == ope2;
														variable.tipo = "Boolean";
													}else{
														Double ope1 = Double.valueOf(valor1.valor.toString());
														Character ope2 = (Character)valor2.valor;
														variable.valor = ope1 == ope2.hashCode();
														variable.tipo = "Boolean";
													}
												}else if( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 == ope2;
													variable.tipo = "Boolean";
												}else{
													// Error String Numerico
												}
											}
											break;
										case "tNoIgual":
											if(valor1.tipo.equals(valor2.tipo)){
												if(valor1.tipo.equals("String")){
													String ope1 = (String)valor1.valor;
													String ope2 = (String)valor2.valor;
													int n1 = 0;
													int n2 = 0;
													for (int i = 0; i < ope1.length(); i++) {
														n1 += ope1.charAt(i);
													}
													for (int i = 0; i < ope2.length(); i++) {
														n2 += ope2.charAt(i);
													}
													variable.valor = n1 != n2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Integer")){
													Integer ope1 = (Integer)valor1.valor;
													Integer ope2 = (Integer)valor2.valor;
													variable.valor = ope1 != ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Double")){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 != ope2;
													variable.tipo = "Boolean";
												}else if(valor1.tipo.equals("Character")){
													Character ope1 = (Character)valor1.valor;
													Character ope2 = (Character)valor2.valor;
													variable.valor = ope1 != ope2;
													variable.tipo = "Boolean";
												}
											}else{
												if( ((valor1.tipo.equals("String") || valor2.tipo.equals("String")) && (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) )) {
													if(valor1.tipo.equals("String")){
														String ope1 = (String)valor1.valor;
														Character ope2 = (Character)valor2.valor;
														int n1 = 0;
														for (int i = 0; i < ope1.length(); i++) {
															n1 += ope1.charAt(i);
														}
														variable.valor = n1 != ope2.hashCode();
														variable.tipo = "Boolean";
													}else{
														Character ope1 = (Character)valor1.valor;
														String ope2 = (String)valor2.valor;
														int n2 = 0;
														for (int i = 0; i < ope2.length(); i++) {
															n2 += ope2.charAt(i);
														}
														variable.valor = ope1.hashCode() != n2;
														variable.tipo = "Boolean";
													}
												}else if(( (valor1.tipo.equals("Character") || valor2.tipo.equals("Character")) && ( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ) ) ){
													 if(valor1.tipo.equals("Character")){
														Character ope1 = (Character)valor1.valor;
														Double ope2 = Double.valueOf(valor2.valor.toString());
														variable.valor = ope1.hashCode() != ope2;
														variable.tipo = "Boolean";
													}else{
														Double ope1 = Double.valueOf(valor1.valor.toString());
														Character ope2 = (Character)valor2.valor;
														variable.valor = ope1 != ope2.hashCode();
														variable.tipo = "Boolean";
													}
												}else if( ( valor1.tipo.equals("Integer") || valor2.tipo.equals("Integer") ) || ( valor1.tipo.equals("Double") || valor2.tipo.equals("Double") ) ){
													Double ope1 = Double.valueOf(valor1.valor.toString());
													Double ope2 = Double.valueOf(valor2.valor.toString());
													variable.valor = ope1 != ope2;
													variable.tipo = "Boolean";
												}else{
													// Error String Numerico
												}
											}
											break;
									}
								}else{
									// Error no relacionar Booleanos
								}
							}
						}else{

							Variable valor1 = new Variable();
							Variable valor2 = new Variable();
							valor1 = CONDICION(padre.hijos.get(0), valor1);
							valor2 = CONDICION(padre.hijos.get(2), valor2);

							if(valor1.tipo.equals("Boolean") && valor2.tipo.equals("Boolean")){
								switch(padre.hijos.get(1).token){
									case "tOr": // CONDICION ::= CONDICION tOr CONDICION
										variable.valor = (Boolean)valor1.valor || (Boolean)valor2.valor;
										variable.tipo = "Boolean";
										break;
									case "tXor":	// CONDICION ::= CONDICION tXor CONDICION
										variable.valor = (Boolean)valor1.valor ^ (Boolean)valor2.valor;
										variable.tipo = "Boolean";
										break;
									case "tAnd":	// CONDICION ::= CONDICION tAnd CONDICION
										variable.valor = (Boolean)valor1.valor && (Boolean)valor2.valor;
										variable.tipo = "Boolean";
										break;
								}
							}else{
								// Error solo boleanos
							}
						}
						break;
					case 2: // CONDICION ::= tNot CONDICION
						Variable valor1 = new Variable();
						valor1 = CONDICION(padre.hijos.get(1), valor1);
						if(valor1.tipo.equals("Boolean")){
							variable.valor = !Boolean.valueOf(valor1.valor.toString());
							variable.tipo = "Boolean";
						}else{
							// Error solo boleanos
						}
						break;
					case 1: // CONDICION ::= E 
						valor1 = new Variable();
                        valor1 = E(padre.hijos.get(0), valor1);
						variable.valor = valor1.valor;
						variable.tipo = valor1.tipo;
						break;
				}
				break;
		}
		return variable;
	}

	public void agregarError(String tipo, String lexema, String comentario, int fila, int columna){ 
		this.erroresLexicos.add(tipo+","+lexema+","+comentario+","+String.valueOf(fila)+","+String.valueOf(columna));
	}
}
