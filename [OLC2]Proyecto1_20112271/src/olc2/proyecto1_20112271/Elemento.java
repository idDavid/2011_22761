package olc2.proyecto1_20112271;


public class Elemento{
    public String tipo;             // Tipo:    [Terminal]  [NoTerminal]
    public String clase;            // Clase:    [Int]  [String]  [Clase1]
    public String nombre;           // Nombre:    [E]  [S]
    public String alias;            // Alias:    [$1]  [$n]
    public String puntoIsq;         // puntoIsq:    [.E]  [.S]
    public String puntoDer;         // puntoDer:    [E.]  [S.]
    public Nodo codIsq;             // codIsq:    <:print():> E
    public Nodo codDer;             // codDer:    E <:print():>
    public String cadenaIsq;        // cadenaIsq:    <sprint() E 
    public String cadenaDer;        // cadenaDer:    E <print()


    public Elemento(String tipo, String clase, String nombre, String alias, String puntoIsq, String puntoDer){
        this.tipo = tipo;
        this.clase = clase;
        this.nombre = nombre;
        this.alias = alias;
        this.puntoIsq = puntoIsq;
        this.puntoDer = puntoDer;
        this.codIsq = null;
        this.codDer = null;
        this.cadenaIsq = "";
        this.cadenaDer = "";
    }
}

