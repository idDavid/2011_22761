package olc2.proyecto1_20112271;

import java.util.ArrayList;

public class Estado{
    public int numero;
    public boolean completado;
    public ArrayList<String> gotos;             // Estado,Simbolo:     [0,E [0,id]
    public ArrayList<String> salidas;           // Simbolo-Cantidad:     [E-3] [id-1]
    public ArrayList<Produccion> producciones;
    public String contenido;

    public Estado(int numero, boolean completado, String contenido){
        this.numero = numero;
        this.completado = completado;
        this.gotos = new ArrayList<String>();
        this.salidas = new ArrayList<String>();
        this.producciones = new ArrayList<Produccion>();
        this.contenido = contenido;
    }

    public void generarContenido(){                                                     // Genera el String de las producciones que tiene
        for (int i=0; i<this.producciones.size(); i++) {
            this.contenido += this.producciones.get(i).padre+"->";
            ArrayList<Elemento> cuerpo = this.producciones.get(i).cuerpo;
            for (int j=0; j<cuerpo.size(); j++) {
                Elemento elemento = cuerpo.get(j);
                this.contenido += elemento.puntoIsq+elemento.nombre+elemento.puntoDer;
            }
        }
    }

    public void addSalida(String simbolo){                                              // Agrega una nueva salida si esta ya existe le aumenta el contador
        boolean existe = false;
        for (int i=0; i<this.salidas.size(); i++) {
            String split[] = this.salidas.get(i).split("-");
            if(split[0].equals(simbolo)){
                int j = Integer.parseInt(split[1]);
                j++;
                this.salidas.set(i,split[0]+"-"+String.valueOf(j));
                existe = true;
                break;
            }
        }

        if(!existe){
            this.salidas.add(simbolo+"-1");
            this.completado = false;
        }
    }

    public String getSalida(){                                                          // Obtiene la salida del indice cero y la elimina
        String simbolo =  "";
        if(this.salidas.size() != 0){
            simbolo = this.salidas.remove(0);
        }
        
        return simbolo;
    }

    public Produccion getProduccion(String simbolo){                                    // Optiene la produccion que no a sido movida a un nuevo estado
        Produccion produccion = null;
        int contador = 0;
        if(!this.completado){
            for (int i=0; i<this.producciones.size(); i++) {
                if(!this.producciones.get(i).completado){
                    if(produccion == null){
                        ArrayList<Elemento> cuerpo = this.producciones.get(i).cuerpo;
                        for (int j=0; j<cuerpo.size(); j++) {
                            Elemento elemento = cuerpo.get(j);
                            if(elemento.puntoIsq.equals(".") && elemento.nombre.equals(simbolo)){
                                produccion = new Produccion(this.producciones.get(i).reduccion, this.producciones.get(i).padre, false);
                                for(int n=0; n<cuerpo.size(); n++){
                                    Elemento nuevoElemento = new Elemento(cuerpo.get(n).tipo,cuerpo.get(n).clase,cuerpo.get(n).nombre,cuerpo.get(n).alias,cuerpo.get(n).puntoIsq,cuerpo.get(n).puntoDer);
                                    produccion.cuerpo.add(nuevoElemento);
                                }
                                this.producciones.get(i).completado = true;
                                contador++;
                                break;
                            }
                        }
                    }
                }else{
                    contador++;
                }
            }
            if(produccion!=null){
                if(contador == this.producciones.size()){
                    this.completado = true;
                }
            }
        }

        return produccion;
    }

}