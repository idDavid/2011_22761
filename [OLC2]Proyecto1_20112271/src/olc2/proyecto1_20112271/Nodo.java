package olc2.proyecto1_20112271;

import java.util.ArrayList;

public class Nodo {
    public String token;
    public String lexema;
    public int fila;
    public int columna;
    public ArrayList<Nodo> hijos;
    /******************************** Nuevos atributos Metodo de Pila y Arbol de Analisis Sintactico ******************************************/
    public String tipo;             // Tipo:    [estado]  [entrada] [noTerminal]
    public String clase;            // Clase:    [Int]  [String]  [Clase1]
    public String alias;            // Alias:    [$1]  [$n]
    public Nodo codIsq;             // codIsq:    <:print():> E
    public Nodo codDer;             // codDer:    E <:print():>    
    public String cadenaIsq;        // cadenaIsq:    <sprint() E 
    public String cadenaDer;        // cadenaDer:    E <print()
    
    public Nodo(String token,String lexema, int fila, int columna){
        this.token = token;
        this.lexema = lexema;
        this.fila = fila;
        this.columna = columna;
        this.hijos = new ArrayList<Nodo>();
        this.tipo =  "";
        this.clase =  "";
        this.alias =  "";
        this.codIsq =  null;
        this.codDer =  null;
        this.cadenaIsq = "";
        this.cadenaDer = "";
    }
    
    public void AddHijo(Nodo nuevo){
        this.hijos.add(nuevo);
    }
    
}
