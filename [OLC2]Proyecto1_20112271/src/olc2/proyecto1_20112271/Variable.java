package olc2.proyecto1_20112271;

import java.util.ArrayList;

public class Variable {
	public String nombre = "";
	public String tipo = "";
	public String ambito = "";
	public String acceso = "";
	public boolean iniciado = false;
	public ArrayList<Variable> globales = new ArrayList<Variable>();
	public ArrayList<Variable> locales = new ArrayList<Variable>();
	public ArrayList<Variable> atributos = new ArrayList<Variable>();
	public Object valor = null;
}
