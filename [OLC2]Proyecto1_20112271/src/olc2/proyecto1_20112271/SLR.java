package olc2.proyecto1_20112271;

import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javafx.scene.Node;

/*** El Metodo SLR: 
        Usa gramaticas No Ambiguas (Solo si se declara precedencia y asociatividad para resolver los conflictos).
        Metodo ascendente (Recursividad por la Izquierda).
        Puede no estar factorisada.        
***/
public class SLR{
    public ArrayList<String> listaTerminales;       // Nombre-Tipo:     [tMas-String] [tNumero-Int]
    public ArrayList<String> listaNoTerminales;     // Nombre-Tipo:     [E-Clase1]  [S-Int]
    public ArrayList<String> listaPresedencia;      // Nombre:          [tMas,tMenos]  [tPOr,tDiv] [tPot]
    public ArrayList<String> listaAsociatividad;    // Nombre-Lado:     [tMas-Izquierda]  [tPOr-Izquierda]
    public String inicial;                          // Simbolo Inicial
    public ArrayList<Produccion> gramatica;
    public ArrayList<String> listaPrimeros;         // Nombre,Simbolos: [E,$,(,id]  [F,(,id]
    public ArrayList<String> listaSiguientes;       // Nombre,Simbolos: [E,$,(,id]  [F,(,id]
    public ArrayList<Estado> listaEstados;
    public String tablaDeTransiciones[][];
    /*************************************************** Nuevos atributos Validacion de Cadena ************************************************************/
    public ArrayList<Nodo> cadenaEntrada;
    public ArrayList<Nodo> pila;
    public String cadenaPila;
    public String conflictos;

    public SLR(){
        this.listaTerminales = new ArrayList<String>();
        this.listaNoTerminales = new ArrayList<String>();
        this.listaPresedencia = new ArrayList<String>();
        this.listaAsociatividad = new ArrayList<String>();
        this.inicial = "";
        this.gramatica = new ArrayList<Produccion>();
        this.listaPrimeros = new ArrayList<String>();
        this.listaSiguientes = new ArrayList<String>();
        this.listaEstados = new ArrayList<Estado>();
        this.cadenaEntrada = new ArrayList<Nodo>();
        this.pila = new ArrayList<Nodo>();
        this.cadenaPila = "";
    }

    public void EstadoCero(){                                                   // Recorre la gramatica original y realisa el Estado Cero coloando toda la informacion posible (#Reduccion, tipoElemento, asilias, etc)
        Estado estadoCero = new Estado(0,false,"");
        ArrayList<Produccion> producciones = new ArrayList<Produccion>();
        Produccion sInicial = new Produccion(0,"$",false);
        Elemento inicio = new Elemento("NoTerminal","",this.inicial,"$1",".","");
        estadoCero.addSalida(this.inicial);
        sInicial.cuerpo.add(inicio);
        producciones.add(sInicial);

        for(int i=0; i<this.gramatica.size(); i++){
            this.gramatica.get(i).reduccion = i+1;
            ArrayList<Elemento> cuerpo = this.gramatica.get(i).cuerpo;
            cuerpo.get(0).puntoIsq = ".";
            estadoCero.addSalida(cuerpo.get(0).nombre);
            for(int j=0; j<cuerpo.size(); j++){
                for(int n=0; n<this.listaTerminales.size(); n++){
                    String split[] = this.listaTerminales.get(n).split("-");
                    if(cuerpo.get(j).nombre.equals(split[0])){
                        cuerpo.get(j).tipo = "Terminal";
                        cuerpo.get(j).clase = split[1];
                        cuerpo.get(j).alias = "$"+(j+1);
                    }
                }
                if(cuerpo.get(j).tipo.equals("")){
                    for(int n=0; n<this.listaNoTerminales.size(); n++){
                        String split[] = this.listaNoTerminales.get(n).split("-");
                        if(cuerpo.get(j).nombre.equals(split[0])){
                            cuerpo.get(j).tipo = "NoTerminal";
                            cuerpo.get(j).clase = split[1];
                            cuerpo.get(j).alias = "$"+(j+1);
                        }
                    }
                }
            }
            producciones.add(this.gramatica.get(i));
        }
        estadoCero.producciones = producciones;
        estadoCero.generarContenido();

        this.listaEstados.add(estadoCero);
    }

    public String Primeros(String noTerminal, String primeros){
        ArrayList<Produccion> producciones = this.listaEstados.get(0).producciones;         // Se toma las gramatic expandida (producciones) del Estado Cero
        for(int i=0; i<producciones.size(); i++){                                           // Se recorren las producciones
            if(noTerminal.equals(producciones.get(i).padre)){                               // Si el productor es el NoTerminal al que se le quiere encontrar los primeros
                Elemento elemento = producciones.get(i).cuerpo.get(0);                      // Primer elemento de la produccion
                if(elemento.tipo.equals("Terminal")){                                       // Si es terminal
                    if(!primeros.contains(elemento.nombre+",")){                            // Si aun no existe el terminal en los primeros se agrega
                        primeros += elemento.nombre+",";
                    }
                }else if(!elemento.nombre.equals(noTerminal)){                              // Si el NoTerminal es diferente al padre se mete a las pruducciones de el
                    primeros = Primeros(elemento.nombre, primeros);
                }
            }
        }
        return primeros;
    }

    public String Siguientes(ArrayList<String> noTerminal, String siguientes){
        ArrayList<Produccion> producciones = this.listaEstados.get(0).producciones;         // Se toma las gramatic expandida (producciones) del Estado Cero
        for(int i=0; i<producciones.size(); i++){                                           // Se recorren las producciones
            ArrayList<Elemento> cuerpo = producciones.get(i).cuerpo;                        // Se toma el cuerpo de una de las producciones
            for(int j=0; j<cuerpo.size(); j++){                                             // Se recorre cada elemento de la produccion
                if(noTerminal.get(noTerminal.size()-1).equals(cuerpo.get(j).nombre)){                                // Si el elemento es el NoTerminal
                    if(j == cuerpo.size()-1){                                               // Si es el ultimo elemento de la produccion [A -> B] - S(B): S(B) U S(A)
                        if(producciones.get(i).padre.equals("$")){                          // Si es el NoTerminal incial se coloca $ en sus siguientes
                            if(!siguientes.contains("$"+",")){                              // Si aun no existe el terminal en los siguentes se agrega
                                siguientes += "$"+",";
                            }
                        }else{                                                              // [A -> B] - S(B): S(B) U S(A)
                            if(!producciones.get(i).padre.equals(noTerminal.get(noTerminal.size()-1))){              // Que sea diferente al padre
                                for(int n=0; n<noTerminal.size(); n++){
                                    if(noTerminal.get(n).equals(producciones.get(i).padre)){
                                        return siguientes;
                                    }
                                }
                                noTerminal.add(producciones.get(i).padre);
                                siguientes = Siguientes(noTerminal,siguientes);
                            }
                        }
                    }else if(cuerpo.get(j+1).tipo.equals("Terminal")){                      // Si el siguiente elemnto a la derecha es terminal [A -> Ba] - S(B): S(B) U a
                        if(!siguientes.contains(cuerpo.get(j+1).nombre+",")){               // Si aun no existe el terminal en los siguentes se agrega
                            siguientes += cuerpo.get(j+1).nombre+",";
                        }
                    }else{                                                                  // Si el siguiente elemento a la derecha es no terminal [A -> BCa] - S(B): S(B) U P(C)
                        for(int n=0; n<this.listaPrimeros.size(); n++){
                            String split[] = this.listaPrimeros.get(n).split(",");
                            if(cuerpo.get(j+1).nombre.equals(split[0])){
                                for(int x=1; x<split.length; x++){
                                    if(!siguientes.contains(split[x]+",")){   // Si aun no existe el terminal en los siguentes se agrega S(B): S(B) U P(C)
                                        siguientes += split[x]+",";
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        return siguientes;
    }

    public void PrimerosSiguientes(){
        this.listaEstados.clear();
        this.listaPrimeros.clear();
        this.listaSiguientes.clear();
        EstadoCero();

        /******************************************************************************************* Calculo de Primeros */
        for(int i=0; i<this.listaNoTerminales.size(); i++){
            String split[] = this.listaNoTerminales.get(i).split("-");
            String primeros = "";
            if(split[0].equals(this.inicial)){    // Si es el NoTerminal incial se coloca $ en sus primeros
                primeros += "$,";
            }
            primeros = Primeros(split[0],primeros);
            this.listaPrimeros.add(split[0]+","+primeros);
        }

         /******************************************************************************************* Calculo de Siguientes */
         this.listaSiguientes.add("$,$");    // Se agrega el siguiente de la expancion de la gramatica $ -> E
         for(int i=0; i<this.listaNoTerminales.size(); i++){
             String split[] = this.listaNoTerminales.get(i).split("-");
             ArrayList<String> noTerminal = new ArrayList<String>();
             noTerminal.add(split[0]);
             String siguientes = "";
             siguientes = Siguientes(noTerminal,siguientes);
             this.listaSiguientes.add(split[0]+","+siguientes);
         }
    }

    public ArrayList<Produccion> derivarNoTerminal(String noTerminal,ArrayList<Produccion> gramatica){
        ArrayList<Produccion> producciones = this.listaEstados.get(0).producciones;         // Se toma las gramatic expandida (producciones) del Estado Cero
        for(int i=0; i<producciones.size(); i++){                                           // Se recorren las producciones
            if(noTerminal.equals(producciones.get(i).padre)){                               // Si el productor es el NoTerminal al que se le quiere encontrar los primeros
                Produccion nuevaProduccion = new Produccion(producciones.get(i).reduccion,producciones.get(i).padre,false);
                ArrayList<Elemento> cuerpo= producciones.get(i).cuerpo;
                for(int n=0; n<cuerpo.size(); n++){
                    Elemento nuevoElemento = new Elemento(cuerpo.get(n).tipo,cuerpo.get(n).clase,cuerpo.get(n).nombre,cuerpo.get(n).alias,cuerpo.get(n).puntoIsq,cuerpo.get(n).puntoDer);
                    nuevaProduccion.cuerpo.add(nuevoElemento);
                }
                gramatica.add(nuevaProduccion);                                             // Primer elemento de la produccion
                Elemento elemento = producciones.get(i).cuerpo.get(0);
                if(elemento.tipo.equals("NoTerminal")){                                     // Si es terminal
                    if(!noTerminal.equals(elemento.nombre)){                                // Si aun no existe el terminal en los primeros se agrega
                        gramatica = derivarNoTerminal(elemento.nombre,gramatica);
                    }
                }
            }
        }
        return gramatica;
    }

    public void estados(){
        int i = 0;
        do{
            Estado estado = this.listaEstados.get(i);
            if(!estado.completado){                                                         // Si el estado no a sido completado
                Estado nuevoEstado = new Estado(this.listaEstados.size(),false,"");         // Se crea un nuevo estado
                String split[] = estado.getSalida().split("-");                             // Se optine el simbolo con el que tiene que hacer goto y el numero de producciones a mover
                for(int j=Integer.parseInt(split[1]); j>0; j--){                    
                    Produccion produccion = estado.getProduccion(split[0]);
                    nuevoEstado.producciones.add(produccion);
                    ArrayList<Elemento> cuerpo = produccion.cuerpo;
                    for(int n=0; n<cuerpo.size(); n++){
                        Elemento elemento = cuerpo.get(n);
                        if(elemento.puntoIsq.equals(".")){
                            if(n == cuerpo.size()-1){                                       // Si es el ultimo elemento se pone a su isq y se marca como reduccion y completado
                                elemento.puntoIsq = "";
                                elemento.puntoDer = ".";
                                produccion.reducido = true;
                                produccion.completado = true;
                                if(nuevoEstado.producciones.size() == 1){
                                    nuevoEstado.completado = true;
                                }
                            }else if(cuerpo.get(n+1).tipo.equals("Terminal")){              // Si a la derecha tiene un Terminal se mueve el punto y se coloca Terminal en las salidas del estado
                                elemento.puntoIsq = "";
                                cuerpo.get(n+1).puntoIsq = ".";
                                nuevoEstado.addSalida(cuerpo.get(n+1).nombre);
                            }else{                                                          // Si a la derecha tien un NoTerminal se mueve el punto y se expande deriva el NoTerminal colocando todos las salidas
                                elemento.puntoIsq = "";
                                cuerpo.get(n+1).puntoIsq = ".";
                                boolean existe = false;
                                for(int x=0; x<nuevoEstado.salidas.size(); x++){            // Bustacar el NoTermianl en las salidas para saber si ya se expandio
                                    if(nuevoEstado.salidas.get(x).split("-")[0].equals(cuerpo.get(n+1).nombre)){
                                        existe = true;
                                        break;
                                    }
                                }
                                nuevoEstado.addSalida(cuerpo.get(n+1).nombre);
                                if(!existe){                                                // Si aun no existe se exapande o deriva el NoTerminal
                                    ArrayList<Produccion> gramatica = new ArrayList<Produccion>();
                                    gramatica = derivarNoTerminal(cuerpo.get(n+1).nombre,gramatica);    // Se debe derivar o expandir el NoTerminal desde el Estado Cero
                                    for(int x=0; x<gramatica.size(); x++){
                                        nuevoEstado.producciones.add(gramatica.get(x));
                                        nuevoEstado.addSalida(gramatica.get(x).cuerpo.get(0).nombre);
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
                nuevoEstado.generarContenido();                                             // Genero el String del contenido para comparar
                boolean existe = false;
                for(int j=0; j<this.listaEstados.size(); j++){                              // Recorro todos los estado existentes
                    if(this.listaEstados.get(j).contenido.equals(nuevoEstado.contenido)){   // Si el estado ya existe
                        this.listaEstados.get(j).gotos.add(String.valueOf(i)+","+split[0]);
                        existe = true;
                        break;
                    }
                }
                if(!existe){                                                                // Si no existe le agrego el goto y guardo el nuevo estado
                    nuevoEstado.gotos.add(String.valueOf(i)+","+split[0]);
                    this.listaEstados.add(nuevoEstado); 
                }
                i=0;
            }else{
                i++;
            }
        }while(i != this.listaEstados.size());
    }
    
    public int indexSimbolo(String simbolo){
        int i = 0;
        for(i=0; i<(this.listaTerminales.size()+1+this.listaNoTerminales.size()); i++){   // Se recorre solamente el encabesado de la tabla (los simbolos)
            if(this.tablaDeTransiciones[0][i].equals(simbolo)){
                return i;
            }
        }
        return i;
    }
    
    public String obtenerSiguientes(String simbolo){
        String siguientes = "";
        for(int i=0; i<this.listaSiguientes.size(); i++){                                           // Recorre la lista de siguientes
            String split[] = this.listaSiguientes.get(i).split(",");
            if(split[0].equals(simbolo)){                                                           // Si encuentra el sibolo retorna los siguientes del simbolo buscado
                siguientes = this.listaSiguientes.get(i);
                break;
            }
        }
        return siguientes;
    }
    
    public void generarTabla(){
        // PrimerosSiguientes();
        // estados();
        this.conflictos = "";
        this.tablaDeTransiciones = new String[this.listaEstados.size()+1][this.listaTerminales.size()+1+this.listaNoTerminales.size()];
        
        for(int fila=0; fila<this.listaEstados.size()+1; fila++){
            for(int columna=0; columna<this.listaTerminales.size()+1+this.listaNoTerminales.size(); columna++){                 // Recorre cada posicion de la tabla y coloca cadena vacia
                this.tablaDeTransiciones[fila][columna] = "";
                if(fila==0){                                                                                        // Encabesado de todos los simbolos
                        if(columna<this.listaTerminales.size()+1){
                            if(columna<this.listaTerminales.size()){
                                String split[] = this.listaTerminales.get(columna).split("-");
                                this.tablaDeTransiciones[fila][columna] = split[0];
                            }else{
                                this.tablaDeTransiciones[fila][columna] = "$";
                            }
                        }else{
                            String split[] = this.listaNoTerminales.get(columna-(this.listaTerminales.size()+1)).split("-");
                            this.tablaDeTransiciones[fila][columna] = split[0];
                        }
                }
            }
        }
        
        for(int i=1; i<this.listaEstados.size(); i++){                                      // Se recorre cada estado menos el Estado Cero para obtener los gotos y las reducciones (kernel) que pueda tener
            for(int j=0; j<this.listaEstados.get(i).gotos.size(); j++){                     // Se recorre la lista de gotos
                String split[] = this.listaEstados.get(i).gotos.get(j).split(",");
                int fila = Integer.parseInt(split[0])+1;
                int columna = indexSimbolo(split[1]);
                if(this.tablaDeTransiciones[fila][columna].equals("")){
                    this.tablaDeTransiciones[fila][columna] = "S"+String.valueOf(i);
                }else{
                    this.tablaDeTransiciones[fila][columna] += "/S"+String.valueOf(i);
                    this.conflictos += split[1]+",";
                }
            }
            for(int j=0; j<this.listaEstados.get(i).producciones.size(); j++){              // Se recorre todas las producciones para ver si se encuentra una produccion completada por reduccion (kernel)
                if(this.listaEstados.get(i).producciones.get(j).reducido){                  // Si fue completado por reduccion se coloca en cada uno de los siguientes del padre reduccion n
                    String split[] = obtenerSiguientes(this.listaEstados.get(i).producciones.get(j).padre).split(",");
                    for(int n=1; n<split.length; n++){                                      // Por cada siguiente del padre se coloca reduccion n
                        int fila = i+1;
                        int columna = indexSimbolo(split[n]);
                        if(this.tablaDeTransiciones[fila][columna].equals("")){
                            this.tablaDeTransiciones[fila][columna] = "R"+String.valueOf(this.listaEstados.get(i).producciones.get(j).reduccion);
                        }else{
                            this.tablaDeTransiciones[fila][columna] += "/R"+String.valueOf(this.listaEstados.get(i).producciones.get(j).reduccion);
                            this.conflictos += split[n]+",";
                        }
                    }
                }
            }
        }
    }
    
    public void verTablaTransiciones(){
        FileWriter archivo = null;
        PrintWriter escritor = null;
        try{
            archivo = new FileWriter("Tabla_Transiciones.csv");
            escritor = new PrintWriter(archivo);
            for(int i=0; i<this.listaEstados.size()+1; i++){
                escritor.print("\n"+(i-1));
                for(int j=0; j<this.listaTerminales.size()+1+this.listaNoTerminales.size(); j++){
                    escritor.print(","+this.tablaDeTransiciones[i][j]);
                }
            }
            escritor.close();
            archivo.close();
            try{
                Thread.sleep(5000);
                try {
                    Desktop.getDesktop().open(new File("Tabla_Transiciones.csv"));
                } catch (IOException ex) {
                    System.out.println("Error " + ex.getMessage());
                }
            }catch(Exception e){
                System.out.println(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int obtenerAsociatividad(String simbolo){
        int presedencia = -1;
        for(int i = 0; i<this.listaAsociatividad.size(); i++){
            String split[] = this.listaAsociatividad.get(i).split("-");
                if(simbolo.equals(split[0])){
                    if(split[1].equals("Izquierda")){
                        return 0;
                    }else{
                        return 1;
                    }
                }
        }
        return presedencia;
    }

    public int obtenerPresedencia(String simbolo){
        int asociatividad = -1;
        for(int i=0,x=this.listaPresedencia.size()-1; i<this.listaPresedencia.size(); i++,x--){
            String split[] = this.listaPresedencia.get(i).split(",");
            for(int j=0; j<split.length; j++){
                if(simbolo.equals(split[j])){
                    return x;
                }
            }
        }
        return asociatividad;
    }

    public void validarCadena(){
        boolean existeError = false;
        ArrayList<String> anteriorActual = new ArrayList<String>();
        Nodo cero = new Nodo("0","",0,0);                                                   // Se agrega el estado cero a la pila y fin de cadean a la cadena de entrada
        cero.tipo = "Estado";
        this.pila.add(cero);
        Nodo fin = new Nodo("$","",0,0);
        this.cadenaEntrada.add(fin);
        do{                                                                                 // Mientras haya cadena de entrada
            Nodo ultimo = this.pila.get(this.pila.size()-1);                                // Obtiene el ultimo elemento de la pila
            if(ultimo.tipo.equals("Estado")){                                               // Si elmento es tipo "Estado"
                Nodo entrada = this.cadenaEntrada.get(0);                                   // Obtener elemento de la cadena
                if(this.conflictos.contains(entrada.token+",")){
                    anteriorActual.add(entrada.token);
                }
                String accion = this.tablaDeTransiciones[Integer.parseInt(ultimo.token)+1][indexSimbolo(entrada.token)];  // Obtener accion de la tabla de transiciones
                this.cadenaPila += "\n"+accionPila()+",("+ultimo.token+"-"+entrada.token+") "+accion;
                if(!accion.equals("")){
                    if(!accion.contains("/")){
                        if(accion.contains("S")){                                           // Si accion es "Shift"
                            entrada = this.cadenaEntrada.remove(0);                         // Quitar elemento de la entrada
                            String split[] = accion.split("S");
                            Nodo shift = new Nodo(split[1],"",0,0);
                            shift.tipo = "Estado";
                            this.pila.add(entrada);                                         // Push elemnto
                            this.pila.add(shift);                                           // Push #shift
                        }else{                                                              // Si accion es "Reduccion"
                            String split[] = accion.split("R");
                            Produccion reduccion = this.listaEstados.get(0).producciones.get(Integer.parseInt(split[1]));   // Obtener reduccion del estado cero
                            Nodo padre = new Nodo(reduccion.padre, "", 0, 0);
                            padre.tipo = "NoTerminal";
                            int j = 0;
                            for(int i=reduccion.cuerpo.size(); i>0; i--, j++){              // Por cada elemento del cuerpo de la reduccion se realisan 2 pop de la pila
                                int index = this.pila.size()-(i*2);
                                Nodo pop = this.pila.remove(index);
                                this.pila.remove(index);
                                pop.clase = reduccion.cuerpo.get(j).clase;
                                pop.alias = reduccion.cuerpo.get(j).alias;
                                pop.codIsq = reduccion.cuerpo.get(j).codIsq;
                                pop.codDer = reduccion.cuerpo.get(j).codDer;
                                pop.cadenaIsq = reduccion.cuerpo.get(j).cadenaIsq;
                                pop.cadenaDer = reduccion.cuerpo.get(j).cadenaDer;
                                padre.AddHijo(pop);                                         // Se agrega uno de los pop al padre de la reduccion
                            }
                            this.pila.add(padre);                                           // Push pila padre de la reduccion
                        }
                    }else{
                        String split[] = accion.split("/");
                        if(!anteriorActual.get(0).equals(entrada.token)){               // Se arreglar conflicto por precedencia
                            int anterior = obtenerPresedencia(anteriorActual.get(0));  // Se optiene la precedencia del anterior
                            int actual = obtenerPresedencia(entrada.token);                 // Se optiene la precedencia del actual
                            if(anterior<actual){                                            // Desplasa
                                accion = split[0];
                                entrada = this.cadenaEntrada.remove(0);                     // Quitar elemento de la entrada
                                split = accion.split("S");
                                Nodo shift = new Nodo(split[1],"",0,0);
                                shift.tipo = "Estado";
                                this.pila.add(entrada);                                     // Push elemnto
                                this.pila.add(shift);                                       // Push #shift
                            }else if(anterior>actual){                                      // Reduce
                                accion = split[1];
                                split = accion.split("R");
                                Produccion reduccion = this.listaEstados.get(0).producciones.get(Integer.parseInt(split[1]));   // Obtener reduccion del estado cero
                                Nodo padre = new Nodo(reduccion.padre, "", 0, 0);
                                padre.tipo = "NoTerminal";
                                int j = 0;
                                for(int i=reduccion.cuerpo.size(); i>0; i--, j++){          // Por cada elemento del cuerpo de la reduccion se realisan 2 pop de la pila
                                    int index = this.pila.size()-(i*2);
                                    Nodo pop = this.pila.remove(index);
                                    this.pila.remove(index);
                                    pop.clase = reduccion.cuerpo.get(j).clase;
                                    pop.alias = reduccion.cuerpo.get(j).alias;
                                    pop.codIsq = reduccion.cuerpo.get(j).codIsq;
                                    pop.codDer = reduccion.cuerpo.get(j).codDer;
                                    padre.AddHijo(pop);                                     // Se agrega uno de los pop al padre de la reduccion
                                }
                                this.pila.add(padre);                                       // Push pila padre de la reduccion
                            }else{                                                          // Precedencias iguales, se arreglar conflicto por asociatividad
                                actual = obtenerAsociatividad(entrada.token);
                                if(actual==1){                                // Desplasa
                                    accion = split[0];
                                    entrada = this.cadenaEntrada.remove(0);                 // Quitar elemento de la entrada
                                    split = accion.split("S");
                                    Nodo shift = new Nodo(split[1],"",0,0);
                                    shift.tipo = "Estado";
                                    this.pila.add(entrada);                                 // Push elemnto
                                    this.pila.add(shift);                                   // Push #shift
                                }else{                                                      // Reduce
                                    accion = split[1];
                                    split = accion.split("R");
                                    Produccion reduccion = this.listaEstados.get(0).producciones.get(Integer.parseInt(split[1]));   // Obtener reduccion del estado cero
                                    Nodo padre = new Nodo(reduccion.padre, "", 0, 0);
                                    padre.tipo = "NoTerminal";
                                    int j = 0;
                                    for(int i=reduccion.cuerpo.size(); i>0; i--, j++){      // Por cada elemento del cuerpo de la reduccion se realisan 2 pop de la pila
                                        int index = this.pila.size()-(i*2);
                                        Nodo pop = this.pila.remove(index);
                                        this.pila.remove(index);
                                        pop.clase = reduccion.cuerpo.get(j).clase;
                                        pop.alias = reduccion.cuerpo.get(j).alias;
                                        pop.codIsq = reduccion.cuerpo.get(j).codIsq;
                                        pop.codDer = reduccion.cuerpo.get(j).codDer;
                                        padre.AddHijo(pop);                                 // Se agrega uno de los pop al padre de la reduccion
                                    }
                                    this.pila.add(padre);                                   // Push pila padre de la reduccion
                                }
                            }
                        }else{                                                              // Se arreglar conflicto por asociatividad
                            int actual = obtenerAsociatividad(entrada.token);
                            if(actual==1){                                                  // Desplasa
                                accion = split[0];
                                entrada = this.cadenaEntrada.remove(0);                     // Quitar elemento de la entrada
                                split = accion.split("S");
                                Nodo shift = new Nodo(split[1],"",0,0);
                                shift.tipo = "Estado";
                                this.pila.add(entrada);                                     // Push elemnto
                                this.pila.add(shift);                                       // Push #shift
                            }else{                                                          // Reduce
                                accion = split[1];
                                split = accion.split("R");
                                Produccion reduccion = this.listaEstados.get(0).producciones.get(Integer.parseInt(split[1]));   // Obtener reduccion del estado cero
                                Nodo padre = new Nodo(reduccion.padre, "", 0, 0);
                                padre.tipo = "NoTerminal";
                                int j = 0;
                                for(int i=reduccion.cuerpo.size(); i>0; i--, j++){          // Por cada elemento del cuerpo de la reduccion se realisan 2 pop de la pila
                                    int index = this.pila.size()-(i*2);
                                    Nodo pop = this.pila.remove(index);
                                    this.pila.remove(index);
                                    pop.clase = reduccion.cuerpo.get(j).clase;
                                    pop.alias = reduccion.cuerpo.get(j).alias;
                                    pop.codIsq = reduccion.cuerpo.get(j).codIsq;
                                    pop.codDer = reduccion.cuerpo.get(j).codDer;
                                    padre.AddHijo(pop);                                     // Se agrega uno de los pop al padre de la reduccion
                                }
                                this.pila.add(padre);                                       // Push pila padre de la reduccion
                            }

                        }
                        anteriorActual.remove(0);
                        // Conflicto
                    }
                }else{
                    existeError = true;
                    // existeError
                }
            }else if(ultimo.tipo.equals("NoTerminal")){
                Nodo penultimo = this.pila.get(this.pila.size()-2);
                if(ultimo.token.equals("$") && penultimo.equals("0")){
                    System.out.print("\n\n=========================== Se acepto la cadena");
                }else{
                    String accion = this.tablaDeTransiciones[Integer.parseInt(penultimo.token)+1][indexSimbolo(ultimo.token)];  // Obtener accion de la tabla de transiciones (penultimo, ultimo)
                    this.cadenaPila += "\n"+accionPila()+",("+penultimo.token+"-"+ultimo.token+") "+accion;
                    if(!accion.equals("")){
                        if(!accion.contains("/")){
                            if(accion.contains("S")){                                       // Si accion es "Shift"
                                String split[] = accion.split("S");
                                Nodo shift = new Nodo(split[1],"",0,0);
                                shift.tipo = "Estado";
                                this.pila.add(shift);                                       // Push #shift
                            }else{
                                // No puede ser
                            }
                        }else{
                            // Conflicto
                        }
                    }else{
                        existeError = true;
                        // existeError
                    }
                }
            }else{
                // No puede ser existeError
                System.out.println("*=========================================================================================== Existe error No puede ser");
            }

        }while(!(this.cadenaEntrada.get(this.cadenaEntrada.size()-1).token.equals("$") && this.pila.get(this.pila.size()-1).token.equals("$")) && !existeError);
        if(!existeError){
            this.cadenaPila += "\n"+accionPila()+","+"Cadena Aceptada";    
        }else{
            this.cadenaPila += "\n"+accionPila()+","+"Error Sintactico"; 
        }

    }
    
    public String accionPila(){
        String pilaEntrada = "";
        for(int i=0; i<this.pila.size(); i++){
            pilaEntrada += this.pila.get(i).token+" ";
        }
        pilaEntrada += ",";
        for(int i=0; i<this.cadenaEntrada.size(); i++){
            pilaEntrada += this.cadenaEntrada.get(i).token+" ";
        }
        return pilaEntrada;
    }
    
    public void verCadenaPila(){
        FileWriter archivo = null;
        PrintWriter escritor = null;
        try{
            archivo = new FileWriter("Acciones_Pila.csv");
            escritor = new PrintWriter(archivo);
            escritor.print("PILA,ENTRADA,ACCIONES");
            escritor.print(this.cadenaPila);
            escritor.close();
            archivo.close();
            try{
                Thread.sleep(5000);
                try {
                    Desktop.getDesktop().open(new File("Acciones_Pila.csv"));
                } catch (IOException ex) {
                    System.out.println("Error " + ex.getMessage());
                }
            }catch(Exception e){
                System.out.println(e);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GraficarArbol(){
        String grafica = "Digraph Arbol_Sintactico_SLR{\n\n" + GraficaNodos(this.pila.get(this.pila.size()-1), "0") + "\n\n}";
        GenerarDot(grafica);
        GenerarBat();
        GenerarJpg();
    }

    private String GraficaNodos(Nodo nodo, String i){
        int k=0; 
        String r = "";
        String nodoTerm = nodo.token;
        nodoTerm = nodoTerm.replace("\"", "");
        r= "node" + i + "[label = \"" + nodoTerm + "\"];\n";
        
        for(int j =0 ; j<=nodo.hijos.size()-1; j++){
            r = r + "node" + i + " -> node" + i + k + "\n";
            r= r + GraficaNodos(nodo.hijos.get(j), ""+i+k);
            k++;
        }
        
        if( !(nodo.lexema.equals("")) ){
            String nodoToken = nodo.lexema;
            nodoToken = nodoToken.replace("\"", "");
            r += "node" + i + "c[label = \"" + nodoToken + "\"];\n";
            r += "node" + i + " -> node" + i + "c\n";
        }
        
        if( !(nodo.cadenaIsq.equals("")) ){
            String nodoIsq = nodo.cadenaIsq;
            nodoIsq = nodoIsq.replace("\"", "");
            r += "node" + i + "i[label = \"" + nodoIsq + "\"];\n";
            r += "node" + i + " -> node" + i + "i\n";
        }

        if( !(nodo.cadenaDer.equals("")) ){
            String nodoDer = nodo.cadenaDer;
            nodoDer = nodoDer.replace("\"", "");
            r += "node" + i + "d[label = \"" + nodoDer + "\"];\n";
            r += "node" + i + " -> node" + i + "d\n";
        }

        return r;
    }

    private void GenerarDot(String cadena){
        FileWriter fichero = null;
        PrintWriter escritor = null;
        try{
            fichero = new FileWriter("Arbol_Sintactico_SLR.dot");
            escritor = new PrintWriter(fichero);
            escritor.println(cadena);
            escritor.close();
            fichero.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GenerarBat(){
        String fic = "Arbol_Sintactico_SLR.bat";
        String dotPath  = "dot.exe";
        String fileInputPath = "Arbol_Sintactico_SLR.dot";
        String fileOutputPath = "Arbol_Sintactico_SLR.jpg";
        String tParam = "-Tjpg";
        String tOParam = "-o";
        
        FileWriter fichero = null;
        PrintWriter escritor = null;
        try{
            fichero = new FileWriter(fic);
            escritor = new PrintWriter(fichero);
            escritor.println("@echo off");
            escritor.println(dotPath + " " + tParam + " " + fileInputPath + " " + tOParam + " " + fileOutputPath);
            escritor.println("exit");
            escritor.close();
            fichero.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void GenerarJpg(){
        Runtime aplicacion = Runtime.getRuntime(); 
        try{
            aplicacion.exec("Arbol_Sintactico_SLR.bat");
            Thread.sleep(10000);
            System.out.println("\n\nEjecuto bat");
            try{
                Thread.sleep(10000);
                try {
                    Desktop.getDesktop().open(new File("Arbol_Sintactico_SLR.jpg"));
                } catch (IOException ex) {
                    System.out.println("Error " + ex.getMessage());
                }
            }catch(Exception e){
                System.out.println(e);
            }
        }catch(Exception e){
            System.out.println(e);
        }
    }
    
}
