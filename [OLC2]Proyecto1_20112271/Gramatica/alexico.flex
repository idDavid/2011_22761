package olc2.proyecto1_20112271;

import java_cup.runtime.*;
import java.io.Reader;
import javax.swing.JOptionPane;
      
%%

%class AnalizadorLexico

%line
%column 
%ignorecase 


%cup


%{

    /***************************************  Generamos un java_cup.symbol para guardar el tipo de token encontrado **********************************************/
    
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /*************************************** Generamos un symbol para el tipo de token encontrado junto con su valor *********************************************/

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }

%}



/*********************************************************** Expresiones Regulares ***********************************************************/

letra= 			[a-zA-ZñÑ]
numero=			[0-9]
entero=			{numero}+
cadena= 		[\"][^\"\n]+[\"]						//"
id=				{letra}({letra}|{numero}|_)*
charr= 			\'[^"\'"]\' 							//'
doble= 			{numero}+(.{numero}+)
comentL=		"//"[^\n]*\n
comentB=		"/*"[^"*/"]*"*/"
archivoUPG=		{id}(".upg")
pathUPG=		"C:\\"({id}"\\")*{archivoUPG}
archivoULX=		{id}(".ulx")
pathULX=		"C:\\"({id}"\\")*{archivoULX}
dolar=			"$"(("$"|{numero}+)(.{id})*)

%%

/**'************************************************************* TOKENS *********************************************************************/

<YYINITIAL> {
"int"						{System.out.print("int "); return symbol(sym.tInt,yytext());}
"string"					{System.out.print("string "); return symbol(sym.tString,yytext());}
"char"						{System.out.print("char "); return symbol(sym.tChar,yytext());}
"double"					{System.out.print("double "); return symbol(sym.tDouble,yytext());}
"bool"						{System.out.print("bool "); return symbol(sym.tBool,yytext());}
"void"						{System.out.print("void "); return symbol(sym.tVoid,yytext());}
"true"						{System.out.print("true "); return symbol(sym.tTrue,yytext());}
"false"						{System.out.print("false "); return symbol(sym.tFalse,yytext());}
"+"							{System.out.print("+ "); return symbol(sym.tMas,yytext());}
"-"							{System.out.print("- "); return symbol(sym.tMenos,yytext());}
"*"							{System.out.print("* "); return symbol(sym.tPor,yytext());}
"/"							{System.out.print("/ "); return symbol(sym.tDiv,yytext());}
"^"							{System.out.print("^ "); return symbol(sym.tPot,yytext());}
"++"						{System.out.print("++ "); return symbol(sym.tIncremento,yytext());}
"--"						{System.out.print("-- "); return symbol(sym.tDecremento,yytext());}
"{"							{System.out.print("{ "); return symbol(sym.tLlaveA,yytext());}
"}"							{System.out.print("} "); return symbol(sym.tLlaveC,yytext());}
"["							{System.out.print("[ "); return symbol(sym.tCorcheteA,yytext());}
"]"							{System.out.print("] "); return symbol(sym.tCorcheteC,yytext());}
"("							{System.out.print("( "); return symbol(sym.tParentesisA,yytext());}
")"							{System.out.print(") "); return symbol(sym.tParentesisC,yytext());}
">"							{System.out.print("> "); return symbol(sym.tMayor,yytext());}
"<"							{System.out.print("< "); return symbol(sym.tMenor,yytext());}
">="						{System.out.print(">= "); return symbol(sym.tMayorIgual,yytext());}
"<="						{System.out.print("<= "); return symbol(sym.tMenorIgual,yytext());}
"=="						{System.out.print("== "); return symbol(sym.tIgual,yytext());}
"!="						{System.out.print("!= "); return symbol(sym.tNoIgual,yytext());}
"||"						{System.out.print("|| "); return symbol(sym.tOr,yytext());}
"??"						{System.out.print("?? "); return symbol(sym.tXor,yytext());}
"&&"						{System.out.print("&& "); return symbol(sym.tAnd,yytext());}
"¡"							{System.out.print("¡ "); return symbol(sym.tNot,yytext());}
";"							{System.out.print(";\n"); return symbol(sym.tPuntoComa,yytext());}
","							{System.out.print(", "); return symbol(sym.tComa,yytext());}
"="							{System.out.print("= "); return symbol(sym.tAsigna,yytext());}
"return"					{System.out.print("return "); return symbol(sym.tReturn,yytext());}
"{:"						{System.out.print("{:\n"); return symbol(sym.tAbre,yytext());}
":}"						{System.out.print(":}\n"); return symbol(sym.tCierra,yytext());}
"class"						{System.out.print("class "); return symbol(sym.tClass,yytext());}
"new"						{System.out.print("new "); return symbol(sym.tNew,yytext());}
"null"						{System.out.print("null "); return symbol(sym.tNull,yytext());}
"."							{System.out.print("."); return symbol(sym.tPunto,yytext());}
"public"					{System.out.print("public "); return symbol(sym.tPublic,yytext());}
"private"					{System.out.print("private "); return symbol(sym.tPrivate,yytext());}
"protected"					{System.out.print("protected "); return symbol(sym.tProtected,yytext());}
"import"					{System.out.print("import "); return symbol(sym.tImport,yytext());}
"extends"					{System.out.print("extends "); return symbol(sym.tExtends,yytext());}
"super"						{System.out.print("super "); return symbol(sym.tSuper,yytext());}
"<!Override!>"				{System.out.print("<!Override!> "); return symbol(sym.tOverride,yytext());}
"THIS"						{System.out.print("THIS "); return symbol(sym.tThis,yytext());}
"if"						{System.out.print("if "); return symbol(sym.tIf,yytext());}
"else"						{System.out.print("else "); return symbol(sym.tElse,yytext());}
"elseif"					{System.out.print("elseif "); return symbol(sym.tElseif,yytext());}
"break"						{System.out.print("break "); return symbol(sym.tBreak,yytext());}
"switch"					{System.out.print("switch "); return symbol(sym.tSwitch,yytext());}
"case"						{System.out.print("case "); return symbol(sym.tCase,yytext());}
":"							{System.out.print(": "); return symbol(sym.tDosPuntos,yytext());}
"default"					{System.out.print("default "); return symbol(sym.tDefault,yytext());}
"continue"					{System.out.print("continue "); return symbol(sym.tContinue,yytext());}
"while"						{System.out.print("while "); return symbol(sym.tWhile,yytext());}
"do"						{System.out.print("do "); return symbol(sym.tDo,yytext());}
"repeat"					{System.out.print("repeat "); return symbol(sym.tRepeat,yytext());}
"until"						{System.out.print("until "); return symbol(sym.tUntil,yytext());}
"for"						{System.out.print("for "); return symbol(sym.tFor,yytext());}
"loop"						{System.out.print("loop "); return symbol(sym.tLoop,yytext());}
"print"						{System.out.print("print "); return symbol(sym.tPrint,yytext());}
"ParseInt"					{System.out.print("ParseInt "); return symbol(sym.tParseInt,yytext());}
"ParseDouble"				{System.out.print("ParseDouble "); return symbol(sym.tParseDouble,yytext());}
"intToSTR"					{System.out.print("intToSTR "); return symbol(sym.tIntToSTR,yytext());}
"doubleToSTR"				{System.out.print("doubleToSTR "); return symbol(sym.tDoubleToSTR,yytext());}
"doubleToInt"				{System.out.print("doubleToInt "); return symbol(sym.tDoubleToInt,yytext());}
"<Dec>"						{System.out.print("<Dec>\n"); return symbol(sym.tDecAbre,yytext());}
"</Dec>"					{System.out.print("</Dec>\n"); return symbol(sym.tDecCierra,yytext());}
"<Gram>"					{System.out.print("<Gram>\n"); return symbol(sym.tGramAbre,yytext());}
"</Gram>"					{System.out.print("</Gram>\n"); return symbol(sym.tGramCierra,yytext());}
"<Cod>"						{System.out.print("<Cod>\n"); return symbol(sym.tCodAbre,yytext());}
"</Cod>"					{System.out.print("</Cod>\n"); return symbol(sym.tCodCierra,yytext());}
"<Terminal>"				{System.out.print("<Terminal> "); return symbol(sym.tTerminalAbre,yytext());}
"</Terminal>"				{System.out.print("</Terminal>\n"); return symbol(sym.tTerminalCierra,yytext());}
"<nonTerminal>"				{System.out.print("<nonTerminal>\n"); return symbol(sym.tNonTerminalAbre,yytext());}
"</nonTerminal>"			{System.out.print("</nonTerminal>\n"); return symbol(sym.tNonTerminalCierra,yytext());}
"<tipo>"					{System.out.print("<tipo> "); return symbol(sym.tTipoAbre,yytext());}
"</tipo>"					{System.out.print("</tipo>\n"); return symbol(sym.tTipoCierra,yytext());}
"<Lista>"					{System.out.print("<Lista>\n"); return symbol(sym.tListaAbre,yytext());}
"</Lista>"					{System.out.print("</Lista>\n"); return symbol(sym.tListaCierra,yytext());}
"<nombre>"					{System.out.print("<nombre> "); return symbol(sym.tNombreAbre,yytext());}
"</nombre>"					{System.out.print("</nombre>\n"); return symbol(sym.tNombreCierra,yytext());}
"<precedencia>"				{System.out.print("<precedencia>\n"); return symbol(sym.tPrecedenciaAbre,yytext());}
"</precedencia>"			{System.out.print("</precedencia>\n"); return symbol(sym.tPrecedenciaCierra,yytext());}
"<asociatividad>"			{System.out.print("<asociatividad>\n"); return symbol(sym.tAsociatividadAbre,yytext());}
"</asociatividad>"			{System.out.print("</asociatividad>\n"); return symbol(sym.tAsociatividadCierra,yytext());}
"<Izquierda>"				{System.out.print("Izquierda "); return symbol(sym.tIzquierda,yytext());}
"<Derecha>"					{System.out.print("Derecha "); return symbol(sym.tDerecha,yytext());}
"<inicio>"					{System.out.print("<inicio>\n"); return symbol(sym.tInicioAbre,yytext());}
"</inicio>"					{System.out.print("</inicio>\n"); return symbol(sym.tInicioCierra,yytext());}
"<sim>"						{System.out.print("<sim> "); return symbol(sym.tSimAbre,yytext());}
"</sim>"					{System.out.print("</sim>\n"); return symbol(sym.tSimCierra,yytext());}
"::="						{System.out.print("::= "); return symbol(sym.tProduce,yytext());}
"|"							{System.out.print("\n| "); return symbol(sym.tPipe,yytext());}
"<:"						{System.out.print("<: "); return symbol(sym.tAccionAbre,yytext());}
":>"						{System.out.print(":> "); return symbol(sym.tAccionCierra,yytext());}
"<tokens>"					{System.out.print("<tokens>\n"); return symbol(sym.tTokensAbre,yytext());}
"</tokens>"					{System.out.print("</tokens>\n"); return symbol(sym.tTokensCierra,yytext());}
"<token>"					{System.out.print("<token>\n"); return symbol(sym.tTokenAbre,yytext());}
"</token>"					{System.out.print("</token>\n"); return symbol(sym.tTokenCierra,yytext());}
"<ListaToken>"				{System.out.print("<ListaToken>\n"); return symbol(sym.tListaTokenAbre,yytext());}
"</ListaToken>"				{System.out.print("</ListaToken>\n"); return symbol(sym.tListaTokenCierra,yytext());}
"<valor>"					{System.out.print("<valor> "); return symbol(sym.tValorAbre,yytext());}
"</valor>"					{System.out.print("</valor>\n"); return symbol(sym.tValorCierra,yytext());}
"<fila>"					{System.out.print("<fila> "); return symbol(sym.tFilaAbre,yytext());}
"</fila>"					{System.out.print("</fila>\n"); return symbol(sym.tFilaCierra,yytext());}
"<columna>"					{System.out.print("<columna> "); return symbol(sym.tColumnaAbre,yytext());}
"</columna>"				{System.out.print("</columna>\n"); return symbol(sym.tColumnaCierra,yytext());}

[ \t\r\f] 					{}
[\n]  						{}
{entero}					{System.out.print(yytext() + " "); return symbol(sym.entero,yytext());}
{cadena}					{System.out.print(yytext() + " "); return symbol(sym.cadena,yytext());}
{charr}						{System.out.print(yytext() + " "); return symbol(sym.charr,yytext());}
{doble}						{System.out.print(yytext() + " "); return symbol(sym.doble,yytext());}
{dolar}						{System.out.print(yytext() + " "); return symbol(sym.dolar,yytext());}
{id}						{System.out.print(yytext() + " "); return symbol(sym.id,yytext());}
{archivoUPG}				{System.out.print(yytext() + " "); return symbol(sym.archivoUPG,yytext());}
{pathUPG}					{System.out.print(yytext() + " "); return symbol(sym.pathUPG,yytext());}
{archivoULX}				{System.out.print(yytext() + " "); return symbol(sym.archivoULX,yytext());}
{pathULX}					{System.out.print(yytext() + " "); return symbol(sym.pathULX,yytext());}
{comentB}					{}
{comentL}					{}

.							{JOptionPane.showMessageDialog(null, "Error Lexico: "+yytext()+" en la linea "+(yyline+1)+" y columna "+(yychar+1));}

}