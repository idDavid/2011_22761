package olc2.proyecto1_20112271;

import java_cup.runtime.*;
import java.io.Reader;
import javax.swing.JOptionPane;
      
%%

%class AnalizadorLexico

%line
%column 
%ignorecase 


%cup


%{

    /***************************************  Generamos un java_cup.symbol para guardar el tipo de token encontrado **********************************************/
    
    private Symbol symbol(int type) {
        return new Symbol(type, yyline, yycolumn);
    }
    
    /*************************************** Generamos un symbol para el tipo de token encontrado junto con su valor *********************************************/

    private Symbol symbol(int type, Object value) {
        return new Symbol(type, yyline, yycolumn, value);
    }

    public static boolean activado = false;
    public static String accion = ""; 

%}



/*********************************************************** Expresiones Regulares ***********************************************************/

letra= 			[a-zA-ZñÑ]
numero=			[0-9]
entero=			{numero}+
cadena= 		[\"][^\"\n]+[\"]|[\"][\"]				//"
id=				{letra}({letra}|{numero}|_)*
charr= 			\'[^"\'"]\' 							//'
doble= 			{numero}+(.{numero}+)
comentL=		"//"[^\n]*\n
comentB=		"/*"[^"*/"]*"*/"
archivoUPG=		{id}(".upg")
pathUPG=		"C:"[^\n]*{archivoUPG}
archivoULX=		{id}(".ulx")
pathULX=		"C:"[^\n]*{archivoULX}
dolar=			"$"(("$"|{numero}+)(.{id})*)
signo= 			"~"|"`"|"@"|"#"|"%"|"&"|"_"|"|"|"\\"|"\""|"'"|"?"
%%

/**'************************************************************* TOKENS *********************************************************************/

<YYINITIAL> {
"int"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "int ";
								}
								System.out.print("int ");
								return symbol(sym.tInt,yytext());
							}
"string"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "string ";
								}
								System.out.print("string ");
								return symbol(sym.tString,yytext());
							}
"char"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "char ";
								}
								System.out.print("char ");
								return symbol(sym.tChar,yytext());
							}
"double"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "double ";
								}
								System.out.print("double ");
								return symbol(sym.tDouble,yytext());
							}
"bool"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "bool ";
								}
								System.out.print("bool ");
								return symbol(sym.tBool,yytext());
							}
"void"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "void ";
								}
								System.out.print("void ");
								return symbol(sym.tVoid,yytext());
							}
"true"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "true ";
								}
								System.out.print("true ");
								return symbol(sym.tTrue,yytext());
							}
"false"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "false ";
								}
								System.out.print("false ");
								return symbol(sym.tFalse,yytext());
							}
"+"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "+ ";
								}
								System.out.print("+ ");
								return symbol(sym.tMas,yytext());
							}
"-"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "- ";
								}
								System.out.print("- ");
								return symbol(sym.tMenos,yytext());
							}
"*"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "* ";
								}
								System.out.print("* ");
								return symbol(sym.tPor,yytext());
							}
"/"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "/ ";
								}
								System.out.print("/ ");
								return symbol(sym.tDiv,yytext());
							}
"^"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "^ ";
								}
								System.out.print("^ ");
								return symbol(sym.tPot,yytext());
							}
"++"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "++ ";
								}
								System.out.print("++ ");
								return symbol(sym.tIncremento,yytext());
							}
"--"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "-- ";
								}
								System.out.print("-- ");
								return symbol(sym.tDecremento,yytext());
							}
"{"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "{ ";
								}
								System.out.print("{ ");
								return symbol(sym.tLlaveA,yytext());
							}
"}"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "} ";
								}
								System.out.print("} ");
								return symbol(sym.tLlaveC,yytext());
							}
"["							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "[ ";
								}
								System.out.print("[ ");
								return symbol(sym.tCorcheteA,yytext());
							}
"]"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "] ";
								}
								System.out.print("] ");
								return symbol(sym.tCorcheteC,yytext());
							}
"("							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "( ";
								}
								System.out.print("( ");
									return symbol(sym.tParentesisA,yytext());
							}
")"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ") ";
								}
								System.out.print(") ");
								return symbol(sym.tParentesisC,yytext());
							}
">"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "> ";
								}
								System.out.print("> ");
								return symbol(sym.tMayor,yytext());
							}
"<"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "< ";
								}
								System.out.print("< ");
								return symbol(sym.tMenor,yytext());
							}
">="						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ">= ";
								}
								System.out.print(">= ");
								return symbol(sym.tMayorIgual,yytext());
							}
"<="						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<= ";
								}
								System.out.print("<= ");
								return symbol(sym.tMenorIgual,yytext());
							}
"=="						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "== ";
								}
								System.out.print("== ");
								return symbol(sym.tIgual,yytext());
							}
"!="						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "!= ";
								}
								System.out.print("!= ");
								return symbol(sym.tNoIgual,yytext());
							}
"||"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "|| ";
								}
								System.out.print("|| ");
								return symbol(sym.tOr,yytext());
							}
"??"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "?? ";
								}
								System.out.print("?? ");
								return symbol(sym.tXor,yytext());
							}
"&&"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "&& ";
								}
								System.out.print("&& ");
								return symbol(sym.tAnd,yytext());
							}
"¡"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "¡ ";
								}
								System.out.print("¡ ");
								return symbol(sym.tNot,yytext());
							}
";"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ";\n ";
								}
								System.out.print(";\n ");
								return symbol(sym.tPuntoComa,yytext());
							}
","							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ", ";
								}
								System.out.print(", ");
								return symbol(sym.tComa,yytext());
							}
"="							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "= ";
								}
								System.out.print("= ");
								return symbol(sym.tAsigna,yytext());
							}
"return"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "return ";
								}
								System.out.print("return ");
								return symbol(sym.tReturn,yytext());
							}
"{:"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "{:\n ";
								}
								System.out.print("{:\n ");
								return symbol(sym.tAbre,yytext());
							}
":}"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ":}\n ";
								}
								System.out.print(":}\n ");
								return symbol(sym.tCierra,yytext());
							}
"class"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "class ";
								}
								System.out.print("class ");
								return symbol(sym.tClass,yytext());
							}
"new"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "new ";
								}
								System.out.print("new ");
								return symbol(sym.tNew,yytext());
							}
"null"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "null ";
								}
								System.out.print("null ");
								return symbol(sym.tNull,yytext());
							}
"."							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ".";
								}
								System.out.print(".");
								return symbol(sym.tPunto,yytext());
							}
"public"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "public ";
								}
								System.out.print("public ");
								return symbol(sym.tPublic,yytext());
							}
"private"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "private ";
								}
								System.out.print("private ");
								return symbol(sym.tPrivate,yytext());
							}
"protected"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "protected ";
								}
								System.out.print("protected ");
								return symbol(sym.tProtected,yytext());
							}
"import"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "import ";
								}
								System.out.print("import ");
								return symbol(sym.tImport,yytext());
							}
"extends"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "extends ";
								}
								System.out.print("extends ");
								return symbol(sym.tExtends,yytext());
							}
"super"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "super ";
								}
								System.out.print("super ");
								return symbol(sym.tSuper,yytext());
							}
"<!Override!>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<!Override!> ";
								}
								System.out.print("<!Override!> ");
								return symbol(sym.tOverride,yytext());
							}
"THIS"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "THIS ";
								}
								System.out.print("THIS ");
								return symbol(sym.tThis,yytext());
							}
"if"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "if ";
								}
								System.out.print("if ");
								return symbol(sym.tIf,yytext());
							}
"else"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "else ";
								}
								System.out.print("else ");
								return symbol(sym.tElse,yytext());
							}
"elseif"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "elseif ";
								}
								System.out.print("elseif ");
								return symbol(sym.tElseif,yytext());
							}
"break"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "break ";
								}
								System.out.print("break ");
								return symbol(sym.tBreak,yytext());
							}
"switch"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "switch ";
								}
								System.out.print("switch ");
								return symbol(sym.tSwitch,yytext());
							}
"case"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "case ";
								}
								System.out.print("case ");
								return symbol(sym.tCase,yytext());
							}
":"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += ": ";
								}
								System.out.print(": ");
								return symbol(sym.tDosPuntos,yytext());
							}
"default"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "default ";
								}
								System.out.print("default ");
								return symbol(sym.tDefault,yytext());
							}
"continue"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "continue ";
								}
								System.out.print("continue ");
								return symbol(sym.tContinue,yytext());
							}
"while"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "while ";
								}
								System.out.print("while ");
								return symbol(sym.tWhile,yytext());
							}
"do"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "do ";
								}
								System.out.print("do ");
								return symbol(sym.tDo,yytext());
							}
"repeat"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "repeat ";
								}
								System.out.print("repeat ");
								return symbol(sym.tRepeat,yytext());
							}
"until"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "until ";
								}
								System.out.print("until ");
								return symbol(sym.tUntil,yytext());
							}
"for"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "for ";
								}
								System.out.print("for ");
								return symbol(sym.tFor,yytext());
							}
"loop"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "loop ";
								}
								System.out.print("loop ");
								return symbol(sym.tLoop,yytext());
							}
"print"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "print ";
								}
								System.out.print("print ");
								return symbol(sym.tPrint,yytext());
							}
"ParseInt"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "ParseInt ";
								}
								System.out.print("ParseInt ");
								return symbol(sym.tParseInt,yytext());
							}
"ParseDouble"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "ParseDouble ";
								}
								System.out.print("ParseDouble ");
								return symbol(sym.tParseDouble,yytext());
							}
"intToSTR"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "intToSTR ";
								}
								System.out.print("intToSTR ");
								return symbol(sym.tIntToSTR,yytext());
							}
"doubleToSTR"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "doubleToSTR ";
								}
								System.out.print("doubleToSTR ");
								return symbol(sym.tDoubleToSTR,yytext());
							}
"doubleToInt"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "doubleToInt ";
								}
								System.out.print("doubleToInt ");
								return symbol(sym.tDoubleToInt,yytext());
							}
"<Dec>"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<Dec>\n ";
								}
								System.out.print("<Dec>\n ");
								return symbol(sym.tDecAbre,yytext());
							}
"</Dec>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</Dec>\n ";
								}
								System.out.print("</Dec>\n ");
								return symbol(sym.tDecCierra,yytext());
							}
"<Gram>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<Gram>\n ";
								}
								System.out.print("<Gram>\n ");
								return symbol(sym.tGramAbre,yytext());
							}
"</Gram>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</Gram>\n ";
								}
								System.out.print("</Gram>\n ");
								return symbol(sym.tGramCierra,yytext());
							}
"<Cod>"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<Cod>\n ";
								}
								System.out.print("<Cod>\n ");
								return symbol(sym.tCodAbre,yytext());
							}
"</Cod>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</Cod>\n ";
								}
								System.out.print("</Cod>\n ");
								return symbol(sym.tCodCierra,yytext());
							}
"<Terminal>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<Terminal> ";
								}
								System.out.print("<Terminal> ");
								return symbol(sym.tTerminalAbre,yytext());
							}
"</Terminal>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</Terminal>\n ";
								}
								System.out.print("</Terminal>\n ");
								return symbol(sym.tTerminalCierra,yytext());
							}
"<nonTerminal>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<nonTerminal>\n ";
								}
								System.out.print("<nonTerminal>\n ");
								return symbol(sym.tNonTerminalAbre,yytext());
							}
"</nonTerminal>"			{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</nonTerminal>\n ";
								}
								System.out.print("</nonTerminal>\n ");
								return symbol(sym.tNonTerminalCierra,yytext());
							}
"<tipo>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<tipo> ";
								}
								System.out.print("<tipo> ");
								return symbol(sym.tTipoAbre,yytext());
							}
"</tipo>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</tipo>\n ";
								}
								System.out.print("</tipo>\n ");
								return symbol(sym.tTipoCierra,yytext());
							}
"<Lista>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<Lista>\n ";
								}
								System.out.print("<Lista>\n ");
								return symbol(sym.tListaAbre,yytext());
							}
"</Lista>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</Lista>\n ";
								}
								System.out.print("</Lista>\n ");
								return symbol(sym.tListaCierra,yytext());
							}
"<nombre>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<nombre> ";
								}
								System.out.print("<nombre> ");
								return symbol(sym.tNombreAbre,yytext());
							}
"</nombre>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</nombre>\n ";
								}
								System.out.print("</nombre>\n ");
								return symbol(sym.tNombreCierra,yytext());
							}
"<precedencia>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<precedencia>\n ";
								}
								System.out.print("<precedencia>\n ");
								return symbol(sym.tPrecedenciaAbre,yytext());
							}
"</precedencia>"			{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</precedencia>\n ";
								}
								System.out.print("</precedencia>\n ");
								return symbol(sym.tPrecedenciaCierra,yytext());
							}
"<asociatividad>"			{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<asociatividad>\n ";
								}
								System.out.print("<asociatividad>\n ");
								return symbol(sym.tAsociatividadAbre,yytext());
							}
"</asociatividad>"			{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</asociatividad>\n ";
								}
								System.out.print("</asociatividad>\n ");
								return symbol(sym.tAsociatividadCierra,yytext());
							}
"<asociacion>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<asociacion> ";
								}
								System.out.print("<asociacion> ");
								return symbol(sym.tAsociacionAbre,yytext());
							}
"</asociacion>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</asociacion>\n ";
								}
								System.out.print("</asociacion>\n ");
								return symbol(sym.tAsociacionCierra,yytext());
							}
"Izquierda"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "Izquierda ";
								}
								System.out.print("Izquierda ");
								return symbol(sym.tIzquierda,yytext());
							}
"Derecha"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "Derecha ";
								}
								System.out.print("Derecha ");
								return symbol(sym.tDerecha,yytext());
							}
"<inicio>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<inicio>\n ";
								}
								System.out.print("<inicio>\n ");
								return symbol(sym.tInicioAbre,yytext());
							}
"</inicio>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</inicio>\n ";
								}
								System.out.print("</inicio>\n ");
								return symbol(sym.tInicioCierra,yytext());
							}
"<sim>"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<sim> ";
								}
								System.out.print("<sim> ");
								return symbol(sym.tSimAbre,yytext());
							}
"</sim>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</sim>\n ";
								}
								System.out.print("</sim>\n ");
								return symbol(sym.tSimCierra,yytext());
							}
"::="						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "::= ";
								}
								System.out.print("::= ");
								return symbol(sym.tProduce,yytext());
							}
"|"							{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "\n| ";
								}
								System.out.print("\n| ");
								return symbol(sym.tPipe,yytext());
							}
"<:"						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<: ";
								}
								AnalizadorLexico.activado = true;
								AnalizadorLexico.accion = "";
								System.out.print("<: ");
								return symbol(sym.tAccionAbre,yytext());
							}
":>"						{	AnalizadorLexico.activado = false;
								System.out.print(":> ");
								return symbol(sym.tAccionCierra,yytext());
							}
"<tokens>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<tokens>\n ";
								}
								System.out.print("<tokens>\n ");
								return symbol(sym.tTokensAbre,yytext());
							}
"</tokens>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</tokens>\n ";
								}
								System.out.print("</tokens>\n ");
								return symbol(sym.tTokensCierra,yytext());
							}
"<token>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<token>\n ";
								}
								System.out.print("<token>\n ");
								return symbol(sym.tTokenAbre,yytext());
							}
"</token>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</token>\n ";
								}
								System.out.print("</token>\n ");
								return symbol(sym.tTokenCierra,yytext());
							}
"<ListaToken>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<ListaToken>\n ";
								}
								System.out.print("<ListaToken>\n ");
								return symbol(sym.tListaTokenAbre,yytext());
							}
"</ListaToken>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</ListaToken>\n ";
								}
								System.out.print("</ListaToken>\n ");
								return symbol(sym.tListaTokenCierra,yytext());
							}
"<valor>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<valor> ";
								}
								System.out.print("<valor> ");
								return symbol(sym.tValorAbre,yytext());
							}
"</valor>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</valor>\n ";
								}
								System.out.print("</valor>\n ");
								return symbol(sym.tValorCierra,yytext());
							}
"<fila>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<fila> ";
								}
								System.out.print("<fila> ");
								return symbol(sym.tFilaAbre,yytext());
							}
"</fila>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</fila>\n ";
								}
								System.out.print("</fila>\n ");
								return symbol(sym.tFilaCierra,yytext());
							}
"<columna>"					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "<columna> ";
								}
								System.out.print("<columna> ");
								return symbol(sym.tColumnaAbre,yytext());
							}
"</columna>"				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "</columna>\n ";
								}
								System.out.print("</columna>\n ");
								return symbol(sym.tColumnaCierra,yytext());
							}


[ \t\r\f] 					{}
[\n]  						{}
{entero}					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.entero,yytext());
							}
{cadena}					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += "\""+yytext()+"\" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.cadena,yytext());
							}
{charr}						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.charr,yytext());
							}
{doble}						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.doble,yytext());
							}
{dolar}						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.dolar,yytext());
							}
{id}						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.id,yytext());
							}
{archivoUPG}				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.archivoUPG,yytext());
							}
{pathUPG}					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.pathUPG,yytext());
							}
{archivoULX}				{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.archivoULX,yytext());
							}
{pathULX}					{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.pathULX,yytext());
							}
{comentB}					{}
{comentL}					{}
{signo}						{	if(AnalizadorLexico.activado){
									AnalizadorLexico.accion += yytext()+" ";
								}
								System.out.print(yytext() + " ");
								return symbol(sym.signo,yytext());
							}

.							{JOptionPane.showMessageDialog(null, "Error Lexico: "+yytext()+" en la linea "+(yyline+1)+" y columna "+(yychar+1));}

}